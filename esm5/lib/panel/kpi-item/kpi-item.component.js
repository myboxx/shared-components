import { __decorate } from "tslib";
import { Component } from '@angular/core';
var KpiItemComponent = /** @class */ (function () {
    function KpiItemComponent() {
    }
    KpiItemComponent.prototype.ngOnInit = function () { };
    KpiItemComponent = __decorate([
        Component({
            selector: 'boxx-kpi-item',
            template: "<p>\n  kpi-item works!\n</p>\n",
            styles: [""]
        })
    ], KpiItemComponent);
    return KpiItemComponent;
}());
export { KpiItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia3BpLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLWNvbXBvbmVudHMvIiwic291cmNlcyI6WyJsaWIvcGFuZWwva3BpLWl0ZW0va3BpLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT2xEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQixtQ0FBUSxHQUFSLGNBQVksQ0FBQztJQUpGLGdCQUFnQjtRQUw1QixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZUFBZTtZQUN6QiwwQ0FBd0M7O1NBRXpDLENBQUM7T0FDVyxnQkFBZ0IsQ0FNNUI7SUFBRCx1QkFBQztDQUFBLEFBTkQsSUFNQztTQU5ZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JveHgta3BpLWl0ZW0nLFxuICB0ZW1wbGF0ZVVybDogJy4va3BpLWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9rcGktaXRlbS5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBLcGlJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge31cblxufVxuIl19