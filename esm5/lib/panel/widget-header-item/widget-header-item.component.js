import { __decorate } from "tslib";
import { Component } from '@angular/core';
var WidgetHeaderItemComponent = /** @class */ (function () {
    function WidgetHeaderItemComponent() {
    }
    WidgetHeaderItemComponent.prototype.ngOnInit = function () { };
    WidgetHeaderItemComponent = __decorate([
        Component({
            selector: 'boxx-widget-header-item',
            template: "<p>\n  widget-header-item works!\n</p>\n",
            styles: [""]
        })
    ], WidgetHeaderItemComponent);
    return WidgetHeaderItemComponent;
}());
export { WidgetHeaderItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LWhlYWRlci1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL3BhbmVsL3dpZGdldC1oZWFkZXItaXRlbS93aWRnZXQtaGVhZGVyLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT2xEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQiw0Q0FBUSxHQUFSLGNBQVksQ0FBQztJQUpGLHlCQUF5QjtRQUxyQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUseUJBQXlCO1lBQ25DLG9EQUFrRDs7U0FFbkQsQ0FBQztPQUNXLHlCQUF5QixDQU1yQztJQUFELGdDQUFDO0NBQUEsQUFORCxJQU1DO1NBTlkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYm94eC13aWRnZXQtaGVhZGVyLWl0ZW0nLFxuICB0ZW1wbGF0ZVVybDogJy4vd2lkZ2V0LWhlYWRlci1pdGVtLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vd2lkZ2V0LWhlYWRlci1pdGVtLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIFdpZGdldEhlYWRlckl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG59XG4iXX0=