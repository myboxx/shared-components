import { __decorate } from "tslib";
import { Component } from '@angular/core';
var AppointmentItemComponent = /** @class */ (function () {
    function AppointmentItemComponent() {
    }
    AppointmentItemComponent.prototype.ngOnInit = function () { };
    AppointmentItemComponent = __decorate([
        Component({
            selector: 'boxx-appointment-item',
            template: "<p>\n  appointment-item works!\n</p>\n",
            styles: [""]
        })
    ], AppointmentItemComponent);
    return AppointmentItemComponent;
}());
export { AppointmentItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwb2ludG1lbnQtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtY29tcG9uZW50cy8iLCJzb3VyY2VzIjpbImxpYi9ldmVudHMvYXBwb2ludG1lbnQtaXRlbS9hcHBvaW50bWVudC1pdGVtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU9sRDtJQUVFO0lBQWdCLENBQUM7SUFFakIsMkNBQVEsR0FBUixjQUFZLENBQUM7SUFKRix3QkFBd0I7UUFMcEMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHVCQUF1QjtZQUNqQyxrREFBZ0Q7O1NBRWpELENBQUM7T0FDVyx3QkFBd0IsQ0FNcEM7SUFBRCwrQkFBQztDQUFBLEFBTkQsSUFNQztTQU5ZLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JveHgtYXBwb2ludG1lbnQtaXRlbScsXG4gIHRlbXBsYXRlVXJsOiAnLi9hcHBvaW50bWVudC1pdGVtLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vYXBwb2ludG1lbnQtaXRlbS5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBBcHBvaW50bWVudEl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG59XG4iXX0=