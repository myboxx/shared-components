import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
var HiddenInputItemComponent = /** @class */ (function () {
    function HiddenInputItemComponent() {
        this.whenInputVisibilityChange = new EventEmitter();
        this.inputShown = false;
    }
    HiddenInputItemComponent.prototype.ngOnInit = function () { };
    HiddenInputItemComponent.prototype.toggleRealInputVisibility = function (show) {
        this.inputShown = show;
        this.whenInputVisibilityChange.emit(show);
    };
    __decorate([
        Input()
    ], HiddenInputItemComponent.prototype, "text", void 0);
    __decorate([
        Output()
    ], HiddenInputItemComponent.prototype, "closeIconName", void 0);
    __decorate([
        Output()
    ], HiddenInputItemComponent.prototype, "closeIconSize", void 0);
    __decorate([
        Output()
    ], HiddenInputItemComponent.prototype, "closeIconSlot", void 0);
    __decorate([
        Output()
    ], HiddenInputItemComponent.prototype, "whenInputVisibilityChange", void 0);
    HiddenInputItemComponent = __decorate([
        Component({
            selector: 'boxx-hidden-input-item',
            template: "<div class=\"main-input-wrapper\">\n    <ion-item [hidden]=\"inputShown\" class=\"fake-input boxx-bg-transparent\" lines=\"none\">\n        <ion-label (click)=\"toggleRealInputVisibility(true)\">\n            {{text}}\n        </ion-label>\n    </ion-item>\n    <ion-item class=\"real-input boxx-bg-transparent\" [hidden]=\"!inputShown\" lines=\"none\">\n        <ng-content select=\"ion-input\"></ng-content>\n        <ng-content select=\"ion-input | ion-textarea\"></ng-content>\n        <ion-icon class=\"boxx-hidden-input-close-icon\" [name]=\"closeIconName || 'close-circle'\"\n            [size]=\"closeIconSize || 'large'\" (click)=\"toggleRealInputVisibility(false)\"\n            [slot]=\"closeIconSlot || 'end'\">\n        </ion-icon>\n    </ion-item>\n</div>",
            styles: [".fake-input{font-size:21px;font-weight:700;color:var(--fakeinput-icon-color,var(--ion-color-secondary));text-align:var(--fakeinput-align,center);color:var(--fakeinput-color,var(--ion-color-secondary))}.real-input ::ng-deep ion-input,.real-input ::ng-deep ion-textarea{font-size:21px;font-weight:700;color:var(--realinput-color,var(--ion-color-secondary));text-align:var(--realinput-align,center)}.real-input ion-icon#boxx-hidden-input-close-icon{width:29px!important;height:29px!important;margin-top:var(--realinput-icon-margin-top,19px);margin-bottom:var(--realinput-icon-margin-bottom,18px);color:var(--realinput-icon-color,var(--ion-color-secondary))}ion-item{--padding-start:24px;--padding-end:24px;--inner-padding-end:var(--item-inner-padding-end, 0)}"]
        })
    ], HiddenInputItemComponent);
    return HiddenInputItemComponent;
}());
export { HiddenInputItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlkZGVuLWlucHV0LWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLWNvbXBvbmVudHMvIiwic291cmNlcyI6WyJsaWIvb25ib2FyZGluZy9oaWRkZW4taW5wdXQtaXRlbS9oaWRkZW4taW5wdXQtaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPL0U7SUFTSTtRQUpVLDhCQUF5QixHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7UUFFbEUsZUFBVSxHQUFHLEtBQUssQ0FBQztJQUVILENBQUM7SUFFakIsMkNBQVEsR0FBUixjQUFhLENBQUM7SUFFZCw0REFBeUIsR0FBekIsVUFBMEIsSUFBYTtRQUNuQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUV2QixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFoQlE7UUFBUixLQUFLLEVBQUU7MERBQWM7SUFDWjtRQUFULE1BQU0sRUFBRTttRUFBdUI7SUFDdEI7UUFBVCxNQUFNLEVBQUU7bUVBQThDO0lBQzdDO1FBQVQsTUFBTSxFQUFFO21FQUFnQztJQUMvQjtRQUFULE1BQU0sRUFBRTsrRUFBeUQ7SUFMekQsd0JBQXdCO1FBTHBDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSx3QkFBd0I7WUFDbEMsNndCQUFpRDs7U0FFcEQsQ0FBQztPQUNXLHdCQUF3QixDQWtCcEM7SUFBRCwrQkFBQztDQUFBLEFBbEJELElBa0JDO1NBbEJZLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWhpZGRlbi1pbnB1dC1pdGVtJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vaGlkZGVuLWlucHV0LWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2hpZGRlbi1pbnB1dC1pdGVtLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIEhpZGRlbklucHV0SXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgdGV4dDogc3RyaW5nO1xuICAgIEBPdXRwdXQoKSBjbG9zZUljb25OYW1lOiBzdHJpbmc7XG4gICAgQE91dHB1dCgpIGNsb3NlSWNvblNpemU6ICdzbWFsbCcgfCAnbGFyZ2UnIHwgdW5kZWZpbmVkO1xuICAgIEBPdXRwdXQoKSBjbG9zZUljb25TbG90OiAnc3RhcnQnIHwgJ2VuZCc7XG4gICAgQE91dHB1dCgpIHdoZW5JbnB1dFZpc2liaWxpdHlDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XG5cbiAgICBpbnB1dFNob3duID0gZmFsc2U7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7IH1cblxuICAgIHRvZ2dsZVJlYWxJbnB1dFZpc2liaWxpdHkoc2hvdzogYm9vbGVhbikge1xuICAgICAgICB0aGlzLmlucHV0U2hvd24gPSBzaG93O1xuXG4gICAgICAgIHRoaXMud2hlbklucHV0VmlzaWJpbGl0eUNoYW5nZS5lbWl0KHNob3cpO1xuICAgIH1cbn1cbiJdfQ==