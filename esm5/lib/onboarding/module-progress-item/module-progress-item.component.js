import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var ModuleProgressItemComponent = /** @class */ (function () {
    function ModuleProgressItemComponent() {
        this.value = 0; // progress value, from 0 to 1
    }
    ModuleProgressItemComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], ModuleProgressItemComponent.prototype, "text", void 0);
    __decorate([
        Input()
    ], ModuleProgressItemComponent.prototype, "class", void 0);
    __decorate([
        Input()
    ], ModuleProgressItemComponent.prototype, "progressbarColor", void 0);
    __decorate([
        Input()
    ], ModuleProgressItemComponent.prototype, "value", void 0);
    ModuleProgressItemComponent = __decorate([
        Component({
            selector: 'boxx-module-progress-item',
            template: "<div class=\"main-wrapper\" [ngClass]=\"class\">\n    <div class=\"progress-wrapper\">\n        <div class=\"text\">{{text}}</div>\n        <ion-progress-bar [color]=\"progressbarColor || 'success'\" [value]=\"value\"></ion-progress-bar>\n    </div>\n    <ng-content></ng-content>\n</div>",
            styles: [".main-wrapper{margin-top:var(--boxx-progress-container-margin-top,24px);margin-bottom:var(--boxx-progress-container-margin-bottom,32px);margin-left:var(--boxx-progress-container-margin-left,16px);margin-right:var(--boxx-progress-container-margin-right,16px);color:var(--boxx-progress-container-color,var(--ion-color-primary));text-align:var(--boxx-progress-container-align,center);font-size:15px;font-weight:600}.main-wrapper .progress-wrapper{color:var(--boxx-progress-text-color,#fff);text-align:var(--boxx-progress-text-align,right);font-size:12px;font-weight:500}.main-wrapper .progress-wrapper .text{padding:var(--boxx-progress-text-padding,0);margin:var(--boxx-progress-text-margin,0 0 4px 0)}.main-wrapper .progress-wrapper ion-progress-bar{height:var(--boxx-progress-bar-height,8px);background:var(--boxx-progress-bar-background,#fff)}.main-wrapper ::ng-deep ion-button{margin:var(--boxx-progress-button-margin,16px 0)}.main-wrapper ::ng-deep ion-button:last-of-type{margin-bottom:32px}"]
        })
    ], ModuleProgressItemComponent);
    return ModuleProgressItemComponent;
}());
export { ModuleProgressItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kdWxlLXByb2dyZXNzLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLWNvbXBvbmVudHMvIiwic291cmNlcyI6WyJsaWIvb25ib2FyZGluZy9tb2R1bGUtcHJvZ3Jlc3MtaXRlbS9tb2R1bGUtcHJvZ3Jlc3MtaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT3pEO0lBTUk7UUFGUyxVQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsOEJBQThCO0lBRWxDLENBQUM7SUFFakIsOENBQVEsR0FBUixjQUFhLENBQUM7SUFQTDtRQUFSLEtBQUssRUFBRTs2REFBYztJQUNiO1FBQVIsS0FBSyxFQUFFOzhEQUFlO0lBQ2Q7UUFBUixLQUFLLEVBQUU7eUVBQTBCO0lBQ3pCO1FBQVIsS0FBSyxFQUFFOzhEQUFXO0lBSlYsMkJBQTJCO1FBTHZDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSwyQkFBMkI7WUFDckMsNFNBQW9EOztTQUV2RCxDQUFDO09BQ1csMkJBQTJCLENBVXZDO0lBQUQsa0NBQUM7Q0FBQSxBQVZELElBVUM7U0FWWSwyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LW1vZHVsZS1wcm9ncmVzcy1pdGVtJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vbW9kdWxlLXByb2dyZXNzLWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL21vZHVsZS1wcm9ncmVzcy1pdGVtLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIE1vZHVsZVByb2dyZXNzSXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgdGV4dDogc3RyaW5nO1xuICAgIEBJbnB1dCgpIGNsYXNzOiBzdHJpbmc7XG4gICAgQElucHV0KCkgcHJvZ3Jlc3NiYXJDb2xvcjogc3RyaW5nO1xuICAgIEBJbnB1dCgpIHZhbHVlID0gMDsgLy8gcHJvZ3Jlc3MgdmFsdWUsIGZyb20gMCB0byAxXG5cbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7IH1cblxufVxuIl19