import { __decorate } from "tslib";
import { Component } from '@angular/core';
var ConversationItemComponent = /** @class */ (function () {
    function ConversationItemComponent() {
    }
    ConversationItemComponent.prototype.ngOnInit = function () { };
    ConversationItemComponent = __decorate([
        Component({
            selector: 'boxx-conversation-item',
            template: "<p>\n  conversation-item works!\n</p>\n",
            styles: [""]
        })
    ], ConversationItemComponent);
    return ConversationItemComponent;
}());
export { ConversationItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udmVyc2F0aW9uLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLWNvbXBvbmVudHMvIiwic291cmNlcyI6WyJsaWIvbWVzc2FnZXMvY29udmVyc2F0aW9uLWl0ZW0vY29udmVyc2F0aW9uLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT2xEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQiw0Q0FBUSxHQUFSLGNBQVksQ0FBQztJQUpGLHlCQUF5QjtRQUxyQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsd0JBQXdCO1lBQ2xDLG1EQUFpRDs7U0FFbEQsQ0FBQztPQUNXLHlCQUF5QixDQU1yQztJQUFELGdDQUFDO0NBQUEsQUFORCxJQU1DO1NBTlkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYm94eC1jb252ZXJzYXRpb24taXRlbScsXG4gIHRlbXBsYXRlVXJsOiAnLi9jb252ZXJzYXRpb24taXRlbS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NvbnZlcnNhdGlvbi1pdGVtLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIENvbnZlcnNhdGlvbkl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG59XG4iXX0=