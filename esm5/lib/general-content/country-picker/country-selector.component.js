import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
var CountryPickerComponent = /** @class */ (function () {
    function CountryPickerComponent(viewController) {
        this.viewController = viewController;
        this.countries = [];
        this.items = [];
    }
    CountryPickerComponent.prototype.ngOnInit = function () {
        this.items = this.countries;
    };
    CountryPickerComponent.prototype.select = function (country) {
        this.viewController.dismiss(country);
    };
    CountryPickerComponent.prototype.search = function (searchTerm) {
        this.items = this.countries.filter(function (country) {
            return (country.translations.es || country.name || '')
                .toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') // replace accented chars
                .indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    CountryPickerComponent.prototype.close = function () {
        this.viewController.dismiss();
    };
    CountryPickerComponent.ctorParameters = function () { return [
        { type: ModalController }
    ]; };
    __decorate([
        Input()
    ], CountryPickerComponent.prototype, "countries", void 0);
    CountryPickerComponent = __decorate([
        Component({
            selector: 'boxx-country-selector',
            template: "<boxx-custom-modal>\n    <div title translate>GENERAL.ACTION.selectCountry</div>\n\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #CountryPicker showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n            (ionChange)=\"search($event.detail.value)\" placeholder=\"{{'GENERAL.ACTION.searchCountry' | translate}}\">\n        </ion-searchbar>\n    </ion-item-divider>\n\n    <ion-virtual-scroll [items]=\"items\" approxItemHeight=\"64px\">\n        <ion-item *virtualItem=\"let country\" (click)=\"select(country)\">\n            <ion-label>\n                <ion-img [src]=\"country.flag\" [alt]=\"country.name\" style=\"width: 24px; display: inline-block;\">\n                </ion-img>\n                {{country.translations.es || country.name}}\n            </ion-label>\n        </ion-item>\n    </ion-virtual-scroll>\n</boxx-custom-modal>",
            styles: [""]
        })
    ], CountryPickerComponent);
    return CountryPickerComponent;
}());
export { CountryPickerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRyeS1zZWxlY3Rvci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtY29tcG9uZW50cy8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmFsLWNvbnRlbnQvY291bnRyeS1waWNrZXIvY291bnRyeS1zZWxlY3Rvci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQVNqRDtJQUtJLGdDQUNXLGNBQStCO1FBQS9CLG1CQUFjLEdBQWQsY0FBYyxDQUFpQjtRQUxqQyxjQUFTLEdBQXlCLEVBQUUsQ0FBQztRQUU5QyxVQUFLLEdBQUcsRUFBRSxDQUFDO0lBSVAsQ0FBQztJQUVMLHlDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDaEMsQ0FBQztJQUVELHVDQUFNLEdBQU4sVUFBTyxPQUFzQjtRQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsdUNBQU0sR0FBTixVQUFPLFVBQWtCO1FBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBQSxPQUFPO1lBQ3RDLE9BQUEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsSUFBSSxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztpQkFDMUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUMsQ0FBQyx5QkFBeUI7aUJBQ3hGLE9BQU8sQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7UUFGM0MsQ0FFMkMsQ0FDOUMsQ0FBQztJQUNOLENBQUM7SUFFRCxzQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNsQyxDQUFDOztnQkFyQjBCLGVBQWU7O0lBTGpDO1FBQVIsS0FBSyxFQUFFOzZEQUFzQztJQURyQyxzQkFBc0I7UUFMbEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHVCQUF1QjtZQUNqQywrNEJBQWdEOztTQUVuRCxDQUFDO09BQ1csc0JBQXNCLENBNEJsQztJQUFELDZCQUFDO0NBQUEsQUE1QkQsSUE0QkM7U0E1Qlksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNb2RhbENvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5pbXBvcnQgeyBJQ291bnRyeUNvZGVzIH0gZnJvbSAnQGJveHgvY29udGFjdHMtY29yZSc7XG5cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWNvdW50cnktc2VsZWN0b3InLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb3VudHJ5LXNlbGVjdG9yLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9jb3VudHJ5LXNlbGVjdG9yLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQ291bnRyeVBpY2tlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgY291bnRyaWVzOiBBcnJheTxJQ291bnRyeUNvZGVzPiA9IFtdO1xuXG4gICAgaXRlbXMgPSBbXTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwdWJsaWMgdmlld0NvbnRyb2xsZXI6IE1vZGFsQ29udHJvbGxlclxuICAgICkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5pdGVtcyA9IHRoaXMuY291bnRyaWVzO1xuICAgIH1cblxuICAgIHNlbGVjdChjb3VudHJ5OiBJQ291bnRyeUNvZGVzKSB7XG4gICAgICAgIHRoaXMudmlld0NvbnRyb2xsZXIuZGlzbWlzcyhjb3VudHJ5KTtcbiAgICB9XG5cbiAgICBzZWFyY2goc2VhcmNoVGVybTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuaXRlbXMgPSB0aGlzLmNvdW50cmllcy5maWx0ZXIoY291bnRyeSA9PlxuICAgICAgICAgICAgKGNvdW50cnkudHJhbnNsYXRpb25zLmVzIHx8IGNvdW50cnkubmFtZSB8fCAnJylcbiAgICAgICAgICAgICAgICAudG9Mb3dlckNhc2UoKS5ub3JtYWxpemUoJ05GRCcpLnJlcGxhY2UoL1tcXHUwMzAwLVxcdTAzNmZdL2csICcnKSAvLyByZXBsYWNlIGFjY2VudGVkIGNoYXJzXG4gICAgICAgICAgICAgICAgLmluZGV4T2Yoc2VhcmNoVGVybS50b0xvd2VyQ2FzZSgpKSA+IC0xXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgY2xvc2UoKSB7XG4gICAgICAgIHRoaXMudmlld0NvbnRyb2xsZXIuZGlzbWlzcygpO1xuICAgIH1cbn1cbiJdfQ==