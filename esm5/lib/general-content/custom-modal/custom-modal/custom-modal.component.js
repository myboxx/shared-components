import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
var CustomModalComponent = /** @class */ (function () {
    function CustomModalComponent(popoverController) {
        this.popoverController = popoverController;
    }
    CustomModalComponent.prototype.ngOnInit = function () { };
    CustomModalComponent.prototype.close = function () {
        this.popoverController.dismiss();
    };
    CustomModalComponent.ctorParameters = function () { return [
        { type: ModalController }
    ]; };
    CustomModalComponent = __decorate([
        Component({
            selector: 'boxx-custom-modal',
            template: "<ion-header class=\"ion-no-border\">\n    <ion-toolbar>\n        <div id=\"grabber\"></div>\n        <ion-title>\n            <ng-content select=\"div[title]\"></ng-content>\n        </ion-title>\n\n        <ion-buttons slot=\"end\">\n            <ion-button (click)=\"close()\">\n                <ion-icon slot=\"icon-only\" name=\"close-circle\" color=\"medium\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ng-content></ng-content>\n</ion-content>",
            styles: [":host{background:var(--boxx-modal-header-background,#01091a)}ion-header ion-toolbar{margin-top:var(--boxx-modal-toolbar-margin-top,45px);margin-bottom:var(--boxx-modal-toolbar-margin-bottom,-2px);border-top-left-radius:var(--boxx-modal-toolbar-topleft-radius,10px);border-top-right-radius:var(--boxx-modal-toolbar-topright-radius,10px);--background:var(--boxx-modal-toolbar-background, var(--boxx-app-background, white));color:var(--boxx-modal-toolbar-color,var(--ion-color-dark))}ion-header ion-toolbar ion-title{margin-left:var(--boxx-modal-title-margin-left,11px)!important;font-size:var(--boxx-modal-title-font-size,17px);font-weight:var(--boxx-modal-title-font-weight,700)}ion-header ion-toolbar ion-buttons[slot=end]{margin-right:var(--boxx-modal-buttons-end-margin-right,4px)}ion-header ion-toolbar #grabber{width:var(--boxx-modal-grabber-width,36px);height:var(--boxx-modal-grabber-height,5px);background:var(--boxx-modal-grabber-background,#c7c7cc);margin:var(--boxx-modal-grabber-margin,0 auto);position:var(--boxx-modal-grabber-position,absolute);top:var(--boxx-modal-grabber-top,7px);left:var(--boxx-modal-grabber-left,15px);right:var(--boxx-modal-grabber-right,0)}ion-content{position:var(--boxx-modal-content-position,absolute);height:var(--boxx-modal-content-height,100%)}"]
        })
    ], CustomModalComponent);
    return CustomModalComponent;
}());
export { CustomModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL2dlbmVyYWwtY29udGVudC9jdXN0b20tbW9kYWwvY3VzdG9tLW1vZGFsL2N1c3RvbS1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBT2pEO0lBRUksOEJBQW9CLGlCQUFrQztRQUFsQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQWlCO0lBQUksQ0FBQztJQUUzRCx1Q0FBUSxHQUFSLGNBQWEsQ0FBQztJQUVkLG9DQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDckMsQ0FBQzs7Z0JBTnNDLGVBQWU7O0lBRjdDLG9CQUFvQjtRQUxoQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLHdoQkFBNEM7O1NBRS9DLENBQUM7T0FDVyxvQkFBb0IsQ0FTaEM7SUFBRCwyQkFBQztDQUFBLEFBVEQsSUFTQztTQVRZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNb2RhbENvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYm94eC1jdXN0b20tbW9kYWwnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jdXN0b20tbW9kYWwuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2N1c3RvbS1tb2RhbC5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBDdXN0b21Nb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBvcG92ZXJDb250cm9sbGVyOiBNb2RhbENvbnRyb2xsZXIpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7IH1cblxuICAgIGNsb3NlKCkge1xuICAgICAgICB0aGlzLnBvcG92ZXJDb250cm9sbGVyLmRpc21pc3MoKTtcbiAgICB9XG59XG4iXX0=