import { __decorate } from "tslib";
import { Component } from '@angular/core';
var ContentDividerComponent = /** @class */ (function () {
    function ContentDividerComponent() {
    }
    ContentDividerComponent.prototype.ngOnInit = function () { };
    ContentDividerComponent = __decorate([
        Component({
            selector: 'boxx-content-divider',
            template: "<div class=\"icon-wrapper\">\n    <ion-icon class=\"horizontal-divider\" src=\"assets/icon/Ornament_HomeSeparator.svg\"></ion-icon>\n</div>",
            styles: [".icon-wrapper{height:var(--boxx-content-divider-height,4px);margin-top:var(--boxx-content-divider-margin-top,22px);margin-bottom:var(--boxx-content-divider-margin-bottom,22px);margin-left:var(--boxx-content-divider-margin-left,16px);margin-right:var(--boxx-content-divider-margin-right,16px);padding:var(--boxx-content-divider-padding,0);line-height:var(--boxx-content-line-height,0)}.icon-wrapper ion-icon{width:var(--boxx-icon-width,100%);height:var(--boxx-icon-height,4px)}:host([position=vertical])>div{border:1px solid #fff;transform:rotate(90deg);transform-origin:left;position:relative;width:100%}"]
        })
    ], ContentDividerComponent);
    return ContentDividerComponent;
}());
export { ContentDividerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1kaXZpZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL2dlbmVyYWwtY29udGVudC9jb250ZW50LWRpdmlkZXIvY29udGVudC1kaXZpZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU9sRDtJQUVFO0lBQWdCLENBQUM7SUFFakIsMENBQVEsR0FBUixjQUFZLENBQUM7SUFKRix1QkFBdUI7UUFMbkMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQyx1SkFBK0M7O1NBRWhELENBQUM7T0FDVyx1QkFBdUIsQ0FNbkM7SUFBRCw4QkFBQztDQUFBLEFBTkQsSUFNQztTQU5ZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JveHgtY29udGVudC1kaXZpZGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbnRlbnQtZGl2aWRlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NvbnRlbnQtZGl2aWRlci5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBDb250ZW50RGl2aWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHt9XG5cbn1cbiJdfQ==