import { __decorate } from "tslib";
import { Component } from '@angular/core';
var ContentHeaderItemComponent = /** @class */ (function () {
    function ContentHeaderItemComponent() {
    }
    ContentHeaderItemComponent.prototype.ngOnInit = function () { };
    ContentHeaderItemComponent = __decorate([
        Component({
            selector: 'boxx-content-header-item',
            template: "<div class=\"content-header-wrapper\">\n    <div class=\"icon\">\n        <ng-content select=\"ion-icon\"></ng-content>\n    </div>\n    <div class=\"title\">\n        <ng-content></ng-content>\n    </div>\n</div>",
            styles: [".content-header-wrapper{padding-top:var(--boxx-header-padding-top,0);padding-bottom:var(--boxx-header-padding-bottom,0);background:var(--boxx-header-background,transparent)}.content-header-wrapper .icon{margin-top:var(--boxx-header-icon-margin-top,8px);margin-bottom:var(--boxx-header-icon-margin-bottom,24px);line-height:var(--boxx-header-icon-line-height,0);text-align:var(--boxx-header-icon-text-align,center)}.content-header-wrapper .icon ::ng-deep ion-icon{color:var(--boxx-header-icon-color,var(--ion-color-primary-contrast));width:var(--boxx-header-icon-size,26px);height:var(--boxx-header-icon-size,26px)}.content-header-wrapper .title{margin-top:var(--boxx-header-title-margin-top,24px);margin-bottom:var(--boxx-header-title-margin-bottom,16px);line-height:var(--boxx-header-title-line-height,inherit);text-align:var(--boxx-header-title-text-align,center);font-size:var(--boxx-header-title-font-size,21px);font-weight:var(--boxx-header-title-font-weight,700);color:var(--boxx-header-title-color,var(--ion-color-primary-contrast))}"]
        })
    ], ContentHeaderItemComponent);
    return ContentHeaderItemComponent;
}());
export { ContentHeaderItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1oZWFkZXItaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtY29tcG9uZW50cy8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmFsLWNvbnRlbnQvY29udGVudC1oZWFkZXItaXRlbS9jb250ZW50LWhlYWRlci1pdGVtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU9sRDtJQUVJO0lBQWdCLENBQUM7SUFFakIsNkNBQVEsR0FBUixjQUFhLENBQUM7SUFKTCwwQkFBMEI7UUFMdEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLDBCQUEwQjtZQUNwQyxpT0FBbUQ7O1NBRXRELENBQUM7T0FDVywwQkFBMEIsQ0FNdEM7SUFBRCxpQ0FBQztDQUFBLEFBTkQsSUFNQztTQU5ZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYm94eC1jb250ZW50LWhlYWRlci1pdGVtJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY29udGVudC1oZWFkZXItaXRlbS5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY29udGVudC1oZWFkZXItaXRlbS5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBDb250ZW50SGVhZGVySXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7IH1cblxufVxuIl19