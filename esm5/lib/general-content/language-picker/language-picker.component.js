import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
var LanguagePickerComponent = /** @class */ (function () {
    function LanguagePickerComponent(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.languageList = [];
    }
    LanguagePickerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.languageList = [
            {
                id: 'en',
                name: 'English',
                description: 'English (US)',
                selected: false,
            },
            {
                id: 'es',
                name: 'Spanish',
                description: 'Spanish (Latin America)',
                selected: false,
            },
            {
                id: 'pt',
                name: 'Portuguese',
                description: 'Portugués',
                selected: false,
            },
        ];
        this.languageList.some(function (language) {
            if (language.id === _this.selectedId) {
                language.selected = true;
                return true;
            }
        });
    };
    LanguagePickerComponent.prototype.closeView = function () {
        this.modalCtrl.dismiss();
    };
    LanguagePickerComponent.prototype.selectLanguage = function (language) {
        this.modalCtrl.dismiss(language);
    };
    LanguagePickerComponent.ctorParameters = function () { return [
        { type: ModalController }
    ]; };
    __decorate([
        Input()
    ], LanguagePickerComponent.prototype, "languageList", void 0);
    __decorate([
        Input()
    ], LanguagePickerComponent.prototype, "selectedId", void 0);
    LanguagePickerComponent = __decorate([
        Component({
            selector: 'app-language-picker',
            template: "<boxx-custom-modal>\n    <div title translate>Idioma - Language</div>\n    <ion-list>\n        <ion-item mode=\"ios\" *ngFor=\"let language of languageList\" (click)=\"selectLanguage(language)\">\n            <ion-label position=\"stacked\">language.name</ion-label>\n            <ion-input readonly [value]=\"language.description\"></ion-input>\n            <ion-icon *ngIf=\"language.selected\" slot=\"end\" size=\"small\" color=\"success\"\n                src=\"assets/icon/Icon_NoActiveModule_Step.svg\"></ion-icon>\n        </ion-item>\n    </ion-list>\n</boxx-custom-modal>",
            styles: [""]
        })
    ], LanguagePickerComponent);
    return LanguagePickerComponent;
}());
export { LanguagePickerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFuZ3VhZ2UtcGlja2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL2dlbmVyYWwtY29udGVudC9sYW5ndWFnZS1waWNrZXIvbGFuZ3VhZ2UtcGlja2VyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBT2pEO0lBVUksaUNBQW9CLFNBQTBCO1FBQTFCLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBVHJDLGlCQUFZLEdBS2hCLEVBQUUsQ0FBQztJQUl5QyxDQUFDO0lBRWxELDBDQUFRLEdBQVI7UUFBQSxpQkE0QkM7UUEzQkcsSUFBSSxDQUFDLFlBQVksR0FBRztZQUNoQjtnQkFDSSxFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsU0FBUztnQkFDZixXQUFXLEVBQUUsY0FBYztnQkFDM0IsUUFBUSxFQUFFLEtBQUs7YUFDbEI7WUFDRDtnQkFDSSxFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsU0FBUztnQkFDZixXQUFXLEVBQUUseUJBQXlCO2dCQUN0QyxRQUFRLEVBQUUsS0FBSzthQUNsQjtZQUNEO2dCQUNJLEVBQUUsRUFBRSxJQUFJO2dCQUNSLElBQUksRUFBRSxZQUFZO2dCQUNsQixXQUFXLEVBQUUsV0FBVztnQkFDeEIsUUFBUSxFQUFFLEtBQUs7YUFDbEI7U0FDSixDQUFDO1FBRUYsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQzNCLElBQUksUUFBUSxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQyxRQUFRLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDekIsT0FBTyxJQUFJLENBQUM7YUFDZjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJDQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCxnREFBYyxHQUFkLFVBQWUsUUFBOEU7UUFDekYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDckMsQ0FBQzs7Z0JBdEM4QixlQUFlOztJQVRyQztRQUFSLEtBQUssRUFBRTtpRUFLQTtJQUVDO1FBQVIsS0FBSyxFQUFFOytEQUFvQjtJQVJuQix1QkFBdUI7UUFMbkMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixnbEJBQStDOztTQUVsRCxDQUFDO09BQ1csdUJBQXVCLENBaURuQztJQUFELDhCQUFDO0NBQUEsQUFqREQsSUFpREM7U0FqRFksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNb2RhbENvbnRyb2xsZXIgfSBmcm9tICdAaW9uaWMvYW5ndWxhcic7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYXBwLWxhbmd1YWdlLXBpY2tlcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xhbmd1YWdlLXBpY2tlci5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vbGFuZ3VhZ2UtcGlja2VyLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIExhbmd1YWdlUGlja2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBASW5wdXQoKSBsYW5ndWFnZUxpc3Q6IEFycmF5PHtcbiAgICAgICAgaWQ6IHN0cmluZztcbiAgICAgICAgbmFtZTogc3RyaW5nO1xuICAgICAgICBkZXNjcmlwdGlvbjogc3RyaW5nO1xuICAgICAgICBzZWxlY3RlZDogYm9vbGVhbjtcbiAgICB9PiA9IFtdO1xuXG4gICAgQElucHV0KCkgc2VsZWN0ZWRJZDogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBtb2RhbEN0cmw6IE1vZGFsQ29udHJvbGxlcikge31cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLmxhbmd1YWdlTGlzdCA9IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBpZDogJ2VuJyxcbiAgICAgICAgICAgICAgICBuYW1lOiAnRW5nbGlzaCcsXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246ICdFbmdsaXNoIChVUyknLFxuICAgICAgICAgICAgICAgIHNlbGVjdGVkOiBmYWxzZSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWQ6ICdlcycsXG4gICAgICAgICAgICAgICAgbmFtZTogJ1NwYW5pc2gnLFxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnU3BhbmlzaCAoTGF0aW4gQW1lcmljYSknLFxuICAgICAgICAgICAgICAgIHNlbGVjdGVkOiBmYWxzZSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgaWQ6ICdwdCcsXG4gICAgICAgICAgICAgICAgbmFtZTogJ1BvcnR1Z3Vlc2UnLFxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnUG9ydHVndcOpcycsXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWQ6IGZhbHNlLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgXTtcblxuICAgICAgICB0aGlzLmxhbmd1YWdlTGlzdC5zb21lKGxhbmd1YWdlID0+IHtcbiAgICAgICAgICAgIGlmIChsYW5ndWFnZS5pZCA9PT0gdGhpcy5zZWxlY3RlZElkKSB7XG4gICAgICAgICAgICAgICAgbGFuZ3VhZ2Uuc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBjbG9zZVZpZXcoKSB7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoKTtcbiAgICB9XG5cbiAgICBzZWxlY3RMYW5ndWFnZShsYW5ndWFnZTogeyBpZDogc3RyaW5nOyBuYW1lOiBzdHJpbmc7IGRlc2NyaXB0aW9uOiBzdHJpbmc7IHNlbGVjdGVkOiBib29sZWFuIH0pIHtcbiAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcyhsYW5ndWFnZSk7XG4gICAgfVxufVxuIl19