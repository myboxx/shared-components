import { __awaiter, __decorate, __generator } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';
var LocationPickerComponent = /** @class */ (function () {
    function LocationPickerComponent(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.destroyed$ = new Subject();
        this.searchResults = [];
        this.mapsService = new google.maps.places.AutocompleteService();
    }
    LocationPickerComponent.prototype.ngOnDestroy = function () {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    };
    LocationPickerComponent.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.searchBar.setFocus();
        }, 200);
    };
    LocationPickerComponent.prototype.onInput = function (query) {
        var _this = this;
        this.searchResults = [];
        if (!query) {
            return;
        }
        var config = {
            types: [],
            input: query
        };
        this.mapsService.getPlacePredictions(config, function (predictions, status) {
            if (predictions && predictions.length > 0) {
                predictions.forEach(function (prediction) {
                    _this.searchResults.push(prediction);
                });
            }
            else {
                var locationDesc = {
                    description: query,
                    place_id: 'default_place_id'
                };
                _this.searchResults.push(locationDesc);
            }
        });
    };
    LocationPickerComponent.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.modalCtrl.dismiss( /*{someData: ...} */);
                return [2 /*return*/];
            });
        });
    };
    LocationPickerComponent.prototype.chooseItem = function (item) {
        this.modalCtrl.dismiss({ description: item.description, id: item.place_id });
    };
    LocationPickerComponent.prototype.select = function () { };
    LocationPickerComponent.ctorParameters = function () { return [
        { type: ModalController }
    ]; };
    __decorate([
        ViewChild('placePickerSB', { static: false })
    ], LocationPickerComponent.prototype, "searchBar", void 0);
    LocationPickerComponent = __decorate([
        Component({
            selector: 'boxx-location-picker',
            template: "<boxx-custom-modal>\n    <div title translate>FORM.LABEL.place</div>\n\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #placePickerSB debounce=\"500\" showCancelButton=\"never\"\n            (ionInput)=\"onInput($event.target.value)\" cancelButtonIcon=\"close-circle-outline\"\n            placeholder='{{\"EVENTS.enterAddress\" | translate}}'></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-list>\n        <ion-item mode=\"ios\" *ngFor=\"let item of searchResults\" (click)=\"chooseItem(item)\">\n            <ion-label class=\"ion-text-wrap\">{{ item.description }}</ion-label>\n        </ion-item>\n    </ion-list>\n</boxx-custom-modal>",
            styles: [""]
        })
    ], LocationPickerComponent);
    return LocationPickerComponent;
}());
export { LocationPickerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYXRpb24tcGlja2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL2dlbmVyYWwtY29udGVudC9sb2NhdGlvbi1waWNrZXIvbG9jYXRpb24tcGlja2VyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEUsT0FBTyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMvRCxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBUzNDO0lBWUksaUNBQW9CLFNBQTBCO1FBQTFCLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBVDlDLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBR3BDLGtCQUFhLEdBQWUsRUFBRSxDQUFDO1FBTzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQ3BFLENBQUM7SUFFRCw2Q0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsaURBQWUsR0FBZjtRQUFBLGlCQUlDO1FBSEcsVUFBVSxDQUFDO1lBQ1AsS0FBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM5QixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDWixDQUFDO0lBRUQseUNBQU8sR0FBUCxVQUFRLEtBQWE7UUFBckIsaUJBMEJDO1FBekJHLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBRXhCLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDUixPQUFPO1NBQ1Y7UUFFRCxJQUFNLE1BQU0sR0FBRztZQUNYLEtBQUssRUFBRSxFQUFFO1lBQ1QsS0FBSyxFQUFFLEtBQUs7U0FDZixDQUFDO1FBRUYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsVUFBQyxXQUFXLEVBQUUsTUFBTTtZQUM3RCxJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdkMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFVBQVU7b0JBQzNCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN4QyxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILElBQU0sWUFBWSxHQUFHO29CQUNqQixXQUFXLEVBQUUsS0FBSztvQkFDbEIsUUFBUSxFQUFFLGtCQUFrQjtpQkFDL0IsQ0FBQztnQkFFRixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUN6QztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVLLHVDQUFLLEdBQVg7OztnQkFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBQyxvQkFBb0IsQ0FBQyxDQUFDOzs7O0tBQ2hEO0lBRUQsNENBQVUsR0FBVixVQUFXLElBQVM7UUFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVELHdDQUFNLEdBQU4sY0FBVyxDQUFDOztnQkFuRG1CLGVBQWU7O0lBWEM7UUFBOUMsU0FBUyxDQUFDLGVBQWUsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQzs4REFBeUI7SUFEOUQsdUJBQXVCO1FBTG5DLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsb3JCQUErQzs7U0FFbEQsQ0FBQztPQUNXLHVCQUF1QixDQWdFbkM7SUFBRCw4QkFBQztDQUFBLEFBaEVELElBZ0VDO1NBaEVZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElvblNlYXJjaGJhciwgTW9kYWxDb250cm9sbGVyIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuXG5kZWNsYXJlIGNvbnN0IGdvb2dsZTogYW55O1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtbG9jYXRpb24tcGlja2VyJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vbG9jYXRpb24tcGlja2VyLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9sb2NhdGlvbi1waWNrZXIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBMb2NhdGlvblBpY2tlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XG4gICAgQFZpZXdDaGlsZCgncGxhY2VQaWNrZXJTQicsIHsgc3RhdGljOiBmYWxzZSB9KSBzZWFyY2hCYXI6IElvblNlYXJjaGJhcjtcblxuICAgIGRlc3Ryb3llZCQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuICAgIGlzTG9hZGluZyQ6IE9ic2VydmFibGU8Ym9vbGVhbj47XG5cbiAgICBzZWFyY2hSZXN1bHRzOiBBcnJheTxhbnk+ID0gW107XG5cbiAgICB0cmFuc2xhdGlvbnM6IGFueTtcblxuICAgIG1hcHNTZXJ2aWNlOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIG1vZGFsQ3RybDogTW9kYWxDb250cm9sbGVyKSB7XG4gICAgICAgIHRoaXMubWFwc1NlcnZpY2UgPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLkF1dG9jb21wbGV0ZVNlcnZpY2UoKTtcbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgdGhpcy5kZXN0cm95ZWQkLm5leHQodHJ1ZSk7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5jb21wbGV0ZSgpO1xuICAgIH1cblxuICAgIGlvblZpZXdEaWRFbnRlcigpIHtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNlYXJjaEJhci5zZXRGb2N1cygpO1xuICAgICAgICB9LCAyMDApO1xuICAgIH1cblxuICAgIG9uSW5wdXQocXVlcnk6IHN0cmluZykge1xuICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMgPSBbXTtcblxuICAgICAgICBpZiAoIXF1ZXJ5KSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBjb25maWcgPSB7XG4gICAgICAgICAgICB0eXBlczogW10sIC8vIG90aGVyIHR5cGVzIGF2YWlsYWJsZSBpbiB0aGUgQVBJOiAnYWRkcmVzcyAnZXN0YWJsaXNobWVudCcsICdyZWdpb25zJywgYW5kICdjaXRpZXMnXG4gICAgICAgICAgICBpbnB1dDogcXVlcnlcbiAgICAgICAgfTtcblxuICAgICAgICB0aGlzLm1hcHNTZXJ2aWNlLmdldFBsYWNlUHJlZGljdGlvbnMoY29uZmlnLCAocHJlZGljdGlvbnMsIHN0YXR1cykgPT4ge1xuICAgICAgICAgICAgaWYgKHByZWRpY3Rpb25zICYmIHByZWRpY3Rpb25zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBwcmVkaWN0aW9ucy5mb3JFYWNoKChwcmVkaWN0aW9uKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoUmVzdWx0cy5wdXNoKHByZWRpY3Rpb24pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zdCBsb2NhdGlvbkRlc2MgPSB7XG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBxdWVyeSxcbiAgICAgICAgICAgICAgICAgICAgcGxhY2VfaWQ6ICdkZWZhdWx0X3BsYWNlX2lkJ1xuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMucHVzaChsb2NhdGlvbkRlc2MpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBhc3luYyBjbG9zZSgpIHtcbiAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcygvKntzb21lRGF0YTogLi4ufSAqLyk7XG4gICAgfVxuXG4gICAgY2hvb3NlSXRlbShpdGVtOiBhbnkpIHtcbiAgICAgICAgdGhpcy5tb2RhbEN0cmwuZGlzbWlzcyh7IGRlc2NyaXB0aW9uOiBpdGVtLmRlc2NyaXB0aW9uLCBpZDogaXRlbS5wbGFjZV9pZCB9KTtcbiAgICB9XG5cbiAgICBzZWxlY3QoKSB7IH1cbn1cbiJdfQ==