import { __decorate } from "tslib";
import { Component } from '@angular/core';
var MainHeaderIconComponent = /** @class */ (function () {
    function MainHeaderIconComponent() {
    }
    MainHeaderIconComponent.prototype.ngOnInit = function () { };
    MainHeaderIconComponent = __decorate([
        Component({
            selector: 'boxx-header-icon',
            template: "<div class=\"brand-icon-wrapper\">\n    <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n</div>",
            styles: [".brand-icon-wrapper{position:var(--boxx-brand-icon-wrapper-position,absolute);top:var(--boxx-brand-icon-wrapper-top,0);width:var(--boxx-brand-icon-wrapper-width,100%);text-align:var(--boxx-brand-icon-wrapper-align,center);z-index:var(--boxx-brand-icon-wrapper-z-index,999);line-height:0}.brand-icon-wrapper ion-icon{width:var(--boxx-brand-icon-width,56px);height:var(--boxx-brand-icon-height,20px);color:var(--boxx-brand-icon-color,#fff)}"]
        })
    ], MainHeaderIconComponent);
    return MainHeaderIconComponent;
}());
export { MainHeaderIconComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi1oZWFkZXItaWNvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtY29tcG9uZW50cy8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmFsLWNvbnRlbnQvbWFpbi1oZWFkZXItaWNvbi9tYWluLWhlYWRlci1pY29uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU9sRDtJQUVJO0lBQWdCLENBQUM7SUFFakIsMENBQVEsR0FBUixjQUFhLENBQUM7SUFKTCx1QkFBdUI7UUFMbkMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QiwySEFBZ0Q7O1NBRW5ELENBQUM7T0FDVyx1QkFBdUIsQ0FNbkM7SUFBRCw4QkFBQztDQUFBLEFBTkQsSUFNQztTQU5ZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYm94eC1oZWFkZXItaWNvbicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL21haW4taGVhZGVyLWljb24uY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL21haW4taGVhZGVyLWljb24uY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgTWFpbkhlYWRlckljb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIG5nT25Jbml0KCkgeyB9XG5cbn1cbiJdfQ==