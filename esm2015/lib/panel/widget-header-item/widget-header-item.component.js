import { __decorate } from "tslib";
import { Component } from '@angular/core';
let WidgetHeaderItemComponent = class WidgetHeaderItemComponent {
    constructor() { }
    ngOnInit() { }
};
WidgetHeaderItemComponent = __decorate([
    Component({
        selector: 'boxx-widget-header-item',
        template: "<p>\n  widget-header-item works!\n</p>\n",
        styles: [""]
    })
], WidgetHeaderItemComponent);
export { WidgetHeaderItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LWhlYWRlci1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL3BhbmVsL3dpZGdldC1oZWFkZXItaXRlbS93aWRnZXQtaGVhZGVyLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT2xELElBQWEseUJBQXlCLEdBQXRDLE1BQWEseUJBQXlCO0lBRXBDLGdCQUFnQixDQUFDO0lBRWpCLFFBQVEsS0FBSSxDQUFDO0NBRWQsQ0FBQTtBQU5ZLHlCQUF5QjtJQUxyQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUseUJBQXlCO1FBQ25DLG9EQUFrRDs7S0FFbkQsQ0FBQztHQUNXLHlCQUF5QixDQU1yQztTQU5ZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JveHgtd2lkZ2V0LWhlYWRlci1pdGVtJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3dpZGdldC1oZWFkZXItaXRlbS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3dpZGdldC1oZWFkZXItaXRlbS5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBXaWRnZXRIZWFkZXJJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge31cblxufVxuIl19