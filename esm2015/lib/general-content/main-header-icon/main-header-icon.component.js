import { __decorate } from "tslib";
import { Component } from '@angular/core';
let MainHeaderIconComponent = class MainHeaderIconComponent {
    constructor() { }
    ngOnInit() { }
};
MainHeaderIconComponent = __decorate([
    Component({
        selector: 'boxx-header-icon',
        template: "<div class=\"brand-icon-wrapper\">\n    <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n</div>",
        styles: [".brand-icon-wrapper{position:var(--boxx-brand-icon-wrapper-position,absolute);top:var(--boxx-brand-icon-wrapper-top,0);width:var(--boxx-brand-icon-wrapper-width,100%);text-align:var(--boxx-brand-icon-wrapper-align,center);z-index:var(--boxx-brand-icon-wrapper-z-index,999);line-height:0}.brand-icon-wrapper ion-icon{width:var(--boxx-brand-icon-width,56px);height:var(--boxx-brand-icon-height,20px);color:var(--boxx-brand-icon-color,#fff)}"]
    })
], MainHeaderIconComponent);
export { MainHeaderIconComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi1oZWFkZXItaWNvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9zaGFyZWQtY29tcG9uZW50cy8iLCJzb3VyY2VzIjpbImxpYi9nZW5lcmFsLWNvbnRlbnQvbWFpbi1oZWFkZXItaWNvbi9tYWluLWhlYWRlci1pY29uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU9sRCxJQUFhLHVCQUF1QixHQUFwQyxNQUFhLHVCQUF1QjtJQUVoQyxnQkFBZ0IsQ0FBQztJQUVqQixRQUFRLEtBQUssQ0FBQztDQUVqQixDQUFBO0FBTlksdUJBQXVCO0lBTG5DLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxrQkFBa0I7UUFDNUIsMkhBQWdEOztLQUVuRCxDQUFDO0dBQ1csdUJBQXVCLENBTW5DO1NBTlksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWhlYWRlci1pY29uJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vbWFpbi1oZWFkZXItaWNvbi5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vbWFpbi1oZWFkZXItaWNvbi5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBNYWluSGVhZGVySWNvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7IH1cblxufVxuIl19