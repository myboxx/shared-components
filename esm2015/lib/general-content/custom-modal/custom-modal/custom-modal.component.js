import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
let CustomModalComponent = class CustomModalComponent {
    constructor(popoverController) {
        this.popoverController = popoverController;
    }
    ngOnInit() { }
    close() {
        this.popoverController.dismiss();
    }
};
CustomModalComponent.ctorParameters = () => [
    { type: ModalController }
];
CustomModalComponent = __decorate([
    Component({
        selector: 'boxx-custom-modal',
        template: "<ion-header class=\"ion-no-border\">\n    <ion-toolbar>\n        <div id=\"grabber\"></div>\n        <ion-title>\n            <ng-content select=\"div[title]\"></ng-content>\n        </ion-title>\n\n        <ion-buttons slot=\"end\">\n            <ion-button (click)=\"close()\">\n                <ion-icon slot=\"icon-only\" name=\"close-circle\" color=\"medium\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ng-content></ng-content>\n</ion-content>",
        styles: [":host{background:var(--boxx-modal-header-background,#01091a)}ion-header ion-toolbar{margin-top:var(--boxx-modal-toolbar-margin-top,45px);margin-bottom:var(--boxx-modal-toolbar-margin-bottom,-2px);border-top-left-radius:var(--boxx-modal-toolbar-topleft-radius,10px);border-top-right-radius:var(--boxx-modal-toolbar-topright-radius,10px);--background:var(--boxx-modal-toolbar-background, var(--boxx-app-background, white));color:var(--boxx-modal-toolbar-color,var(--ion-color-dark))}ion-header ion-toolbar ion-title{margin-left:var(--boxx-modal-title-margin-left,11px)!important;font-size:var(--boxx-modal-title-font-size,17px);font-weight:var(--boxx-modal-title-font-weight,700)}ion-header ion-toolbar ion-buttons[slot=end]{margin-right:var(--boxx-modal-buttons-end-margin-right,4px)}ion-header ion-toolbar #grabber{width:var(--boxx-modal-grabber-width,36px);height:var(--boxx-modal-grabber-height,5px);background:var(--boxx-modal-grabber-background,#c7c7cc);margin:var(--boxx-modal-grabber-margin,0 auto);position:var(--boxx-modal-grabber-position,absolute);top:var(--boxx-modal-grabber-top,7px);left:var(--boxx-modal-grabber-left,15px);right:var(--boxx-modal-grabber-right,0)}ion-content{position:var(--boxx-modal-content-position,absolute);height:var(--boxx-modal-content-height,100%)}"]
    })
], CustomModalComponent);
export { CustomModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL2dlbmVyYWwtY29udGVudC9jdXN0b20tbW9kYWwvY3VzdG9tLW1vZGFsL2N1c3RvbS1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBT2pELElBQWEsb0JBQW9CLEdBQWpDLE1BQWEsb0JBQW9CO0lBRTdCLFlBQW9CLGlCQUFrQztRQUFsQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQWlCO0lBQUksQ0FBQztJQUUzRCxRQUFRLEtBQUssQ0FBQztJQUVkLEtBQUs7UUFDRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDckMsQ0FBQztDQUNKLENBQUE7O1lBUDBDLGVBQWU7O0FBRjdDLG9CQUFvQjtJQUxoQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsbUJBQW1CO1FBQzdCLHdoQkFBNEM7O0tBRS9DLENBQUM7R0FDVyxvQkFBb0IsQ0FTaEM7U0FUWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTW9kYWxDb250cm9sbGVyIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JveHgtY3VzdG9tLW1vZGFsJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY3VzdG9tLW1vZGFsLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9jdXN0b20tbW9kYWwuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgQ3VzdG9tTW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBwb3BvdmVyQ29udHJvbGxlcjogTW9kYWxDb250cm9sbGVyKSB7IH1cblxuICAgIG5nT25Jbml0KCkgeyB9XG5cbiAgICBjbG9zZSgpIHtcbiAgICAgICAgdGhpcy5wb3BvdmVyQ29udHJvbGxlci5kaXNtaXNzKCk7XG4gICAgfVxufVxuIl19