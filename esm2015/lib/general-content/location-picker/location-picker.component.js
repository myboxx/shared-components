import { __awaiter, __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { Subject } from 'rxjs';
let LocationPickerComponent = class LocationPickerComponent {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.destroyed$ = new Subject();
        this.searchResults = [];
        this.mapsService = new google.maps.places.AutocompleteService();
    }
    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
    ionViewDidEnter() {
        setTimeout(() => {
            this.searchBar.setFocus();
        }, 200);
    }
    onInput(query) {
        this.searchResults = [];
        if (!query) {
            return;
        }
        const config = {
            types: [],
            input: query
        };
        this.mapsService.getPlacePredictions(config, (predictions, status) => {
            if (predictions && predictions.length > 0) {
                predictions.forEach((prediction) => {
                    this.searchResults.push(prediction);
                });
            }
            else {
                const locationDesc = {
                    description: query,
                    place_id: 'default_place_id'
                };
                this.searchResults.push(locationDesc);
            }
        });
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            this.modalCtrl.dismiss( /*{someData: ...} */);
        });
    }
    chooseItem(item) {
        this.modalCtrl.dismiss({ description: item.description, id: item.place_id });
    }
    select() { }
};
LocationPickerComponent.ctorParameters = () => [
    { type: ModalController }
];
__decorate([
    ViewChild('placePickerSB', { static: false })
], LocationPickerComponent.prototype, "searchBar", void 0);
LocationPickerComponent = __decorate([
    Component({
        selector: 'boxx-location-picker',
        template: "<boxx-custom-modal>\n    <div title translate>FORM.LABEL.place</div>\n\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #placePickerSB debounce=\"500\" showCancelButton=\"never\"\n            (ionInput)=\"onInput($event.target.value)\" cancelButtonIcon=\"close-circle-outline\"\n            placeholder='{{\"EVENTS.enterAddress\" | translate}}'></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-list>\n        <ion-item mode=\"ios\" *ngFor=\"let item of searchResults\" (click)=\"chooseItem(item)\">\n            <ion-label class=\"ion-text-wrap\">{{ item.description }}</ion-label>\n        </ion-item>\n    </ion-list>\n</boxx-custom-modal>",
        styles: [""]
    })
], LocationPickerComponent);
export { LocationPickerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYXRpb24tcGlja2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL2dlbmVyYWwtY29udGVudC9sb2NhdGlvbi1waWNrZXIvbG9jYXRpb24tcGlja2VyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEUsT0FBTyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMvRCxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBUzNDLElBQWEsdUJBQXVCLEdBQXBDLE1BQWEsdUJBQXVCO0lBWWhDLFlBQW9CLFNBQTBCO1FBQTFCLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBVDlDLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBR3BDLGtCQUFhLEdBQWUsRUFBRSxDQUFDO1FBTzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQ3BFLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsZUFBZTtRQUNYLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDWixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzlCLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCxPQUFPLENBQUMsS0FBYTtRQUNqQixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUV4QixJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1IsT0FBTztTQUNWO1FBRUQsTUFBTSxNQUFNLEdBQUc7WUFDWCxLQUFLLEVBQUUsRUFBRTtZQUNULEtBQUssRUFBRSxLQUFLO1NBQ2YsQ0FBQztRQUVGLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLENBQUMsV0FBVyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ2pFLElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN2QyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7b0JBQy9CLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN4QyxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILE1BQU0sWUFBWSxHQUFHO29CQUNqQixXQUFXLEVBQUUsS0FBSztvQkFDbEIsUUFBUSxFQUFFLGtCQUFrQjtpQkFDL0IsQ0FBQztnQkFFRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUN6QztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVLLEtBQUs7O1lBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNqRCxDQUFDO0tBQUE7SUFFRCxVQUFVLENBQUMsSUFBUztRQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUNqRixDQUFDO0lBRUQsTUFBTSxLQUFLLENBQUM7Q0FDZixDQUFBOztZQXBEa0MsZUFBZTs7QUFYQztJQUE5QyxTQUFTLENBQUMsZUFBZSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDOzBEQUF5QjtBQUQ5RCx1QkFBdUI7SUFMbkMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLHNCQUFzQjtRQUNoQyxvckJBQStDOztLQUVsRCxDQUFDO0dBQ1csdUJBQXVCLENBZ0VuQztTQWhFWSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJb25TZWFyY2hiYXIsIE1vZGFsQ29udHJvbGxlciB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QgfSBmcm9tICdyeGpzJztcblxuZGVjbGFyZSBjb25zdCBnb29nbGU6IGFueTtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdib3h4LWxvY2F0aW9uLXBpY2tlcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xvY2F0aW9uLXBpY2tlci5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vbG9jYXRpb24tcGlja2VyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTG9jYXRpb25QaWNrZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xuICAgIEBWaWV3Q2hpbGQoJ3BsYWNlUGlja2VyU0InLCB7IHN0YXRpYzogZmFsc2UgfSkgc2VhcmNoQmFyOiBJb25TZWFyY2hiYXI7XG5cbiAgICBkZXN0cm95ZWQkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcbiAgICBpc0xvYWRpbmckOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuXG4gICAgc2VhcmNoUmVzdWx0czogQXJyYXk8YW55PiA9IFtdO1xuXG4gICAgdHJhbnNsYXRpb25zOiBhbnk7XG5cbiAgICBtYXBzU2VydmljZTogYW55O1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBtb2RhbEN0cmw6IE1vZGFsQ29udHJvbGxlcikge1xuICAgICAgICB0aGlzLm1hcHNTZXJ2aWNlID0gbmV3IGdvb2dsZS5tYXBzLnBsYWNlcy5BdXRvY29tcGxldGVTZXJ2aWNlKCk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMuZGVzdHJveWVkJC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLmRlc3Ryb3llZCQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICBpb25WaWV3RGlkRW50ZXIoKSB7XG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZWFyY2hCYXIuc2V0Rm9jdXMoKTtcbiAgICAgICAgfSwgMjAwKTtcbiAgICB9XG5cbiAgICBvbklucHV0KHF1ZXJ5OiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5zZWFyY2hSZXN1bHRzID0gW107XG5cbiAgICAgICAgaWYgKCFxdWVyeSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgY29uZmlnID0ge1xuICAgICAgICAgICAgdHlwZXM6IFtdLCAvLyBvdGhlciB0eXBlcyBhdmFpbGFibGUgaW4gdGhlIEFQSTogJ2FkZHJlc3MgJ2VzdGFibGlzaG1lbnQnLCAncmVnaW9ucycsIGFuZCAnY2l0aWVzJ1xuICAgICAgICAgICAgaW5wdXQ6IHF1ZXJ5XG4gICAgICAgIH07XG5cbiAgICAgICAgdGhpcy5tYXBzU2VydmljZS5nZXRQbGFjZVByZWRpY3Rpb25zKGNvbmZpZywgKHByZWRpY3Rpb25zLCBzdGF0dXMpID0+IHtcbiAgICAgICAgICAgIGlmIChwcmVkaWN0aW9ucyAmJiBwcmVkaWN0aW9ucy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgcHJlZGljdGlvbnMuZm9yRWFjaCgocHJlZGljdGlvbikgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFJlc3VsdHMucHVzaChwcmVkaWN0aW9uKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbG9jYXRpb25EZXNjID0ge1xuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogcXVlcnksXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlX2lkOiAnZGVmYXVsdF9wbGFjZV9pZCdcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hSZXN1bHRzLnB1c2gobG9jYXRpb25EZXNjKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgY2xvc2UoKSB7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoLyp7c29tZURhdGE6IC4uLn0gKi8pO1xuICAgIH1cblxuICAgIGNob29zZUl0ZW0oaXRlbTogYW55KSB7XG4gICAgICAgIHRoaXMubW9kYWxDdHJsLmRpc21pc3MoeyBkZXNjcmlwdGlvbjogaXRlbS5kZXNjcmlwdGlvbiwgaWQ6IGl0ZW0ucGxhY2VfaWQgfSk7XG4gICAgfVxuXG4gICAgc2VsZWN0KCkgeyB9XG59XG4iXX0=