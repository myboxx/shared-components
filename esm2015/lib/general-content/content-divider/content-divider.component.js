import { __decorate } from "tslib";
import { Component } from '@angular/core';
let ContentDividerComponent = class ContentDividerComponent {
    constructor() { }
    ngOnInit() { }
};
ContentDividerComponent = __decorate([
    Component({
        selector: 'boxx-content-divider',
        template: "<div class=\"icon-wrapper\">\n    <ion-icon class=\"horizontal-divider\" src=\"assets/icon/Ornament_HomeSeparator.svg\"></ion-icon>\n</div>",
        styles: [".icon-wrapper{height:var(--boxx-content-divider-height,4px);margin-top:var(--boxx-content-divider-margin-top,22px);margin-bottom:var(--boxx-content-divider-margin-bottom,22px);margin-left:var(--boxx-content-divider-margin-left,16px);margin-right:var(--boxx-content-divider-margin-right,16px);padding:var(--boxx-content-divider-padding,0);line-height:var(--boxx-content-line-height,0)}.icon-wrapper ion-icon{width:var(--boxx-icon-width,100%);height:var(--boxx-icon-height,4px)}:host([position=vertical])>div{border:1px solid #fff;transform:rotate(90deg);transform-origin:left;position:relative;width:100%}"]
    })
], ContentDividerComponent);
export { ContentDividerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1kaXZpZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3NoYXJlZC1jb21wb25lbnRzLyIsInNvdXJjZXMiOlsibGliL2dlbmVyYWwtY29udGVudC9jb250ZW50LWRpdmlkZXIvY29udGVudC1kaXZpZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQU9sRCxJQUFhLHVCQUF1QixHQUFwQyxNQUFhLHVCQUF1QjtJQUVsQyxnQkFBZ0IsQ0FBQztJQUVqQixRQUFRLEtBQUksQ0FBQztDQUVkLENBQUE7QUFOWSx1QkFBdUI7SUFMbkMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLHNCQUFzQjtRQUNoQyx1SkFBK0M7O0tBRWhELENBQUM7R0FDVyx1QkFBdUIsQ0FNbkM7U0FOWSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdib3h4LWNvbnRlbnQtZGl2aWRlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9jb250ZW50LWRpdmlkZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jb250ZW50LWRpdmlkZXIuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgQ29udGVudERpdmlkZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG59XG4iXX0=