import { __decorate } from "tslib";
import { Component } from '@angular/core';
let ConversationItemComponent = class ConversationItemComponent {
    constructor() { }
    ngOnInit() { }
};
ConversationItemComponent = __decorate([
    Component({
        selector: 'boxx-conversation-item',
        template: "<p>\n  conversation-item works!\n</p>\n",
        styles: [""]
    })
], ConversationItemComponent);
export { ConversationItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udmVyc2F0aW9uLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLWNvbXBvbmVudHMvIiwic291cmNlcyI6WyJsaWIvbWVzc2FnZXMvY29udmVyc2F0aW9uLWl0ZW0vY29udmVyc2F0aW9uLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT2xELElBQWEseUJBQXlCLEdBQXRDLE1BQWEseUJBQXlCO0lBRXBDLGdCQUFnQixDQUFDO0lBRWpCLFFBQVEsS0FBSSxDQUFDO0NBRWQsQ0FBQTtBQU5ZLHlCQUF5QjtJQUxyQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsd0JBQXdCO1FBQ2xDLG1EQUFpRDs7S0FFbEQsQ0FBQztHQUNXLHlCQUF5QixDQU1yQztTQU5ZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JveHgtY29udmVyc2F0aW9uLWl0ZW0nLFxuICB0ZW1wbGF0ZVVybDogJy4vY29udmVyc2F0aW9uLWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jb252ZXJzYXRpb24taXRlbS5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBDb252ZXJzYXRpb25JdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge31cblxufVxuIl19