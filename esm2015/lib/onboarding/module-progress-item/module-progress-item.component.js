import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let ModuleProgressItemComponent = class ModuleProgressItemComponent {
    constructor() {
        this.value = 0; // progress value, from 0 to 1
    }
    ngOnInit() { }
};
__decorate([
    Input()
], ModuleProgressItemComponent.prototype, "text", void 0);
__decorate([
    Input()
], ModuleProgressItemComponent.prototype, "class", void 0);
__decorate([
    Input()
], ModuleProgressItemComponent.prototype, "progressbarColor", void 0);
__decorate([
    Input()
], ModuleProgressItemComponent.prototype, "value", void 0);
ModuleProgressItemComponent = __decorate([
    Component({
        selector: 'boxx-module-progress-item',
        template: "<div class=\"main-wrapper\" [ngClass]=\"class\">\n    <div class=\"progress-wrapper\">\n        <div class=\"text\">{{text}}</div>\n        <ion-progress-bar [color]=\"progressbarColor || 'success'\" [value]=\"value\"></ion-progress-bar>\n    </div>\n    <ng-content></ng-content>\n</div>",
        styles: [".main-wrapper{margin-top:var(--boxx-progress-container-margin-top,24px);margin-bottom:var(--boxx-progress-container-margin-bottom,32px);margin-left:var(--boxx-progress-container-margin-left,16px);margin-right:var(--boxx-progress-container-margin-right,16px);color:var(--boxx-progress-container-color,var(--ion-color-primary));text-align:var(--boxx-progress-container-align,center);font-size:15px;font-weight:600}.main-wrapper .progress-wrapper{color:var(--boxx-progress-text-color,#fff);text-align:var(--boxx-progress-text-align,right);font-size:12px;font-weight:500}.main-wrapper .progress-wrapper .text{padding:var(--boxx-progress-text-padding,0);margin:var(--boxx-progress-text-margin,0 0 4px 0)}.main-wrapper .progress-wrapper ion-progress-bar{height:var(--boxx-progress-bar-height,8px);background:var(--boxx-progress-bar-background,#fff)}.main-wrapper ::ng-deep ion-button{margin:var(--boxx-progress-button-margin,16px 0)}.main-wrapper ::ng-deep ion-button:last-of-type{margin-bottom:32px}"]
    })
], ModuleProgressItemComponent);
export { ModuleProgressItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kdWxlLXByb2dyZXNzLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLWNvbXBvbmVudHMvIiwic291cmNlcyI6WyJsaWIvb25ib2FyZGluZy9tb2R1bGUtcHJvZ3Jlc3MtaXRlbS9tb2R1bGUtcHJvZ3Jlc3MtaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT3pELElBQWEsMkJBQTJCLEdBQXhDLE1BQWEsMkJBQTJCO0lBTXBDO1FBRlMsVUFBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLDhCQUE4QjtJQUVsQyxDQUFDO0lBRWpCLFFBQVEsS0FBSyxDQUFDO0NBRWpCLENBQUE7QUFUWTtJQUFSLEtBQUssRUFBRTt5REFBYztBQUNiO0lBQVIsS0FBSyxFQUFFOzBEQUFlO0FBQ2Q7SUFBUixLQUFLLEVBQUU7cUVBQTBCO0FBQ3pCO0lBQVIsS0FBSyxFQUFFOzBEQUFXO0FBSlYsMkJBQTJCO0lBTHZDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSwyQkFBMkI7UUFDckMsNFNBQW9EOztLQUV2RCxDQUFDO0dBQ1csMkJBQTJCLENBVXZDO1NBVlksMkJBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYm94eC1tb2R1bGUtcHJvZ3Jlc3MtaXRlbScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL21vZHVsZS1wcm9ncmVzcy1pdGVtLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9tb2R1bGUtcHJvZ3Jlc3MtaXRlbS5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBNb2R1bGVQcm9ncmVzc0l0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpIHRleHQ6IHN0cmluZztcbiAgICBASW5wdXQoKSBjbGFzczogc3RyaW5nO1xuICAgIEBJbnB1dCgpIHByb2dyZXNzYmFyQ29sb3I6IHN0cmluZztcbiAgICBASW5wdXQoKSB2YWx1ZSA9IDA7IC8vIHByb2dyZXNzIHZhbHVlLCBmcm9tIDAgdG8gMVxuXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIG5nT25Jbml0KCkgeyB9XG5cbn1cbiJdfQ==