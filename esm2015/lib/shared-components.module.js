import { __decorate } from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CardConfigurationFormComponent } from './business/card-configuration-form/card-configuration-form.component';
import { AppointmentItemComponent } from './events/appointment-item/appointment-item.component';
import { ContentDividerComponent } from './general-content/content-divider/content-divider.component';
import { ContentHeaderItemComponent } from './general-content/content-header-item/content-header-item.component';
import { CountryPickerComponent } from './general-content/country-picker/country-selector.component';
import { CustomModalComponent } from './general-content/custom-modal/custom-modal/custom-modal.component';
import { LanguagePickerComponent } from './general-content/language-picker/language-picker.component';
import { LocationPickerComponent } from './general-content/location-picker/location-picker.component';
import { MainHeaderIconComponent } from './general-content/main-header-icon/main-header-icon.component';
import { ConversationItemComponent } from './messages/conversation-item/conversation-item.component';
import { HiddenInputItemComponent } from './onboarding/hidden-input-item/hidden-input-item.component';
import { ModuleProgressItemComponent } from './onboarding/module-progress-item/module-progress-item.component';
import { KpiItemComponent } from './panel/kpi-item/kpi-item.component';
import { WidgetHeaderItemComponent } from './panel/widget-header-item/widget-header-item.component';
let SharedComponentsModule = class SharedComponentsModule {
};
SharedComponentsModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            IonicModule,
            TranslateModule.forChild()
        ],
        declarations: [
            // ------ GENERAL-CONTENT COMPONENTS:
            MainHeaderIconComponent,
            ContentHeaderItemComponent,
            ContentDividerComponent,
            CustomModalComponent,
            CountryPickerComponent,
            LanguagePickerComponent,
            LocationPickerComponent,
            // ------ BUSINESS COMPONENTS:
            CardConfigurationFormComponent,
            // ------ EVENTS COMPONENTS:
            AppointmentItemComponent,
            // ------ MESSAGES COMPONENTS:
            ConversationItemComponent,
            // ------ ONBOARDING COMPONENTS:
            HiddenInputItemComponent,
            ModuleProgressItemComponent,
            // ------ ONBOARDING COMPONENTS:
            KpiItemComponent,
            WidgetHeaderItemComponent,
        ],
        exports: [
            // ------ GENERAL-CONTENT COMPONENTS:
            MainHeaderIconComponent,
            ContentHeaderItemComponent,
            ContentDividerComponent,
            CustomModalComponent,
            CountryPickerComponent,
            LanguagePickerComponent,
            LocationPickerComponent,
            // ------ BUSINESS COMPONENTS:
            CardConfigurationFormComponent,
            // ------ EVENTS COMPONENTS:
            AppointmentItemComponent,
            // ------ MESSAGES COMPONENTS:
            ConversationItemComponent,
            // ------ ONBOARDING COMPONENTS:
            HiddenInputItemComponent,
            ModuleProgressItemComponent,
            // ------ PANEL COMPONENTS:
            KpiItemComponent,
            WidgetHeaderItemComponent,
        ]
    })
], SharedComponentsModule);
export { SharedComponentsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLWNvbXBvbmVudHMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvc2hhcmVkLWNvbXBvbmVudHMvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkLWNvbXBvbmVudHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsOEJBQThCLEVBQUUsTUFBTSxzRUFBc0UsQ0FBQztBQUN0SCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUNoRyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUN0RyxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxRUFBcUUsQ0FBQztBQUNqSCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUNyRyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxvRUFBb0UsQ0FBQztBQUMxRyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUN0RyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUN0RyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUN4RyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSwwREFBMEQsQ0FBQztBQUNyRyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw0REFBNEQsQ0FBQztBQUN0RyxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSxrRUFBa0UsQ0FBQztBQUMvRyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN2RSxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQW1FcEcsSUFBYSxzQkFBc0IsR0FBbkMsTUFBYSxzQkFBc0I7Q0FBSSxDQUFBO0FBQTFCLHNCQUFzQjtJQWhFbEMsUUFBUSxDQUFDO1FBQ04sT0FBTyxFQUFFO1lBQ0wsWUFBWTtZQUNaLFdBQVc7WUFDWCxXQUFXO1lBQ1gsZUFBZSxDQUFDLFFBQVEsRUFBRTtTQUM3QjtRQUNELFlBQVksRUFBRTtZQUNWLHFDQUFxQztZQUNyQyx1QkFBdUI7WUFDdkIsMEJBQTBCO1lBQzFCLHVCQUF1QjtZQUN2QixvQkFBb0I7WUFDcEIsc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2Qix1QkFBdUI7WUFFdkIsOEJBQThCO1lBQzlCLDhCQUE4QjtZQUU5Qiw0QkFBNEI7WUFDNUIsd0JBQXdCO1lBRXhCLDhCQUE4QjtZQUM5Qix5QkFBeUI7WUFFekIsZ0NBQWdDO1lBQ2hDLHdCQUF3QjtZQUN4QiwyQkFBMkI7WUFFM0IsZ0NBQWdDO1lBQ2hDLGdCQUFnQjtZQUNoQix5QkFBeUI7U0FDNUI7UUFDRCxPQUFPLEVBQUU7WUFDTCxxQ0FBcUM7WUFDckMsdUJBQXVCO1lBQ3ZCLDBCQUEwQjtZQUMxQix1QkFBdUI7WUFDdkIsb0JBQW9CO1lBQ3BCLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsdUJBQXVCO1lBRXZCLDhCQUE4QjtZQUM5Qiw4QkFBOEI7WUFFOUIsNEJBQTRCO1lBQzVCLHdCQUF3QjtZQUV4Qiw4QkFBOEI7WUFDOUIseUJBQXlCO1lBRXpCLGdDQUFnQztZQUNoQyx3QkFBd0I7WUFDeEIsMkJBQTJCO1lBRTNCLDJCQUEyQjtZQUMzQixnQkFBZ0I7WUFDaEIseUJBQXlCO1NBRzVCO0tBQ0osQ0FBQztHQUNXLHNCQUFzQixDQUFJO1NBQTFCLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBJb25pY01vZHVsZSB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHsgQ2FyZENvbmZpZ3VyYXRpb25Gb3JtQ29tcG9uZW50IH0gZnJvbSAnLi9idXNpbmVzcy9jYXJkLWNvbmZpZ3VyYXRpb24tZm9ybS9jYXJkLWNvbmZpZ3VyYXRpb24tZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQXBwb2ludG1lbnRJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9ldmVudHMvYXBwb2ludG1lbnQtaXRlbS9hcHBvaW50bWVudC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb250ZW50RGl2aWRlckNvbXBvbmVudCB9IGZyb20gJy4vZ2VuZXJhbC1jb250ZW50L2NvbnRlbnQtZGl2aWRlci9jb250ZW50LWRpdmlkZXIuY29tcG9uZW50JztcbmltcG9ydCB7IENvbnRlbnRIZWFkZXJJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9nZW5lcmFsLWNvbnRlbnQvY29udGVudC1oZWFkZXItaXRlbS9jb250ZW50LWhlYWRlci1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb3VudHJ5UGlja2VyQ29tcG9uZW50IH0gZnJvbSAnLi9nZW5lcmFsLWNvbnRlbnQvY291bnRyeS1waWNrZXIvY291bnRyeS1zZWxlY3Rvci5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ3VzdG9tTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2dlbmVyYWwtY29udGVudC9jdXN0b20tbW9kYWwvY3VzdG9tLW1vZGFsL2N1c3RvbS1tb2RhbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTGFuZ3VhZ2VQaWNrZXJDb21wb25lbnQgfSBmcm9tICcuL2dlbmVyYWwtY29udGVudC9sYW5ndWFnZS1waWNrZXIvbGFuZ3VhZ2UtcGlja2VyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBMb2NhdGlvblBpY2tlckNvbXBvbmVudCB9IGZyb20gJy4vZ2VuZXJhbC1jb250ZW50L2xvY2F0aW9uLXBpY2tlci9sb2NhdGlvbi1waWNrZXIuY29tcG9uZW50JztcbmltcG9ydCB7IE1haW5IZWFkZXJJY29uQ29tcG9uZW50IH0gZnJvbSAnLi9nZW5lcmFsLWNvbnRlbnQvbWFpbi1oZWFkZXItaWNvbi9tYWluLWhlYWRlci1pY29uLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb252ZXJzYXRpb25JdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9tZXNzYWdlcy9jb252ZXJzYXRpb24taXRlbS9jb252ZXJzYXRpb24taXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHsgSGlkZGVuSW5wdXRJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9vbmJvYXJkaW5nL2hpZGRlbi1pbnB1dC1pdGVtL2hpZGRlbi1pbnB1dC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNb2R1bGVQcm9ncmVzc0l0ZW1Db21wb25lbnQgfSBmcm9tICcuL29uYm9hcmRpbmcvbW9kdWxlLXByb2dyZXNzLWl0ZW0vbW9kdWxlLXByb2dyZXNzLWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEtwaUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL3BhbmVsL2twaS1pdGVtL2twaS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBXaWRnZXRIZWFkZXJJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9wYW5lbC93aWRnZXQtaGVhZGVyLWl0ZW0vd2lkZ2V0LWhlYWRlci1pdGVtLmNvbXBvbmVudCc7XG5cblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIENvbW1vbk1vZHVsZSxcbiAgICAgICAgRm9ybXNNb2R1bGUsXG4gICAgICAgIElvbmljTW9kdWxlLFxuICAgICAgICBUcmFuc2xhdGVNb2R1bGUuZm9yQ2hpbGQoKVxuICAgIF0sXG4gICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgIC8vIC0tLS0tLSBHRU5FUkFMLUNPTlRFTlQgQ09NUE9ORU5UUzpcbiAgICAgICAgTWFpbkhlYWRlckljb25Db21wb25lbnQsXG4gICAgICAgIENvbnRlbnRIZWFkZXJJdGVtQ29tcG9uZW50LFxuICAgICAgICBDb250ZW50RGl2aWRlckNvbXBvbmVudCxcbiAgICAgICAgQ3VzdG9tTW9kYWxDb21wb25lbnQsXG4gICAgICAgIENvdW50cnlQaWNrZXJDb21wb25lbnQsXG4gICAgICAgIExhbmd1YWdlUGlja2VyQ29tcG9uZW50LFxuICAgICAgICBMb2NhdGlvblBpY2tlckNvbXBvbmVudCxcblxuICAgICAgICAvLyAtLS0tLS0gQlVTSU5FU1MgQ09NUE9ORU5UUzpcbiAgICAgICAgQ2FyZENvbmZpZ3VyYXRpb25Gb3JtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBFVkVOVFMgQ09NUE9ORU5UUzpcbiAgICAgICAgQXBwb2ludG1lbnRJdGVtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBNRVNTQUdFUyBDT01QT05FTlRTOlxuICAgICAgICBDb252ZXJzYXRpb25JdGVtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBPTkJPQVJESU5HIENPTVBPTkVOVFM6XG4gICAgICAgIEhpZGRlbklucHV0SXRlbUNvbXBvbmVudCxcbiAgICAgICAgTW9kdWxlUHJvZ3Jlc3NJdGVtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBPTkJPQVJESU5HIENPTVBPTkVOVFM6XG4gICAgICAgIEtwaUl0ZW1Db21wb25lbnQsXG4gICAgICAgIFdpZGdldEhlYWRlckl0ZW1Db21wb25lbnQsXG4gICAgXSxcbiAgICBleHBvcnRzOiBbXG4gICAgICAgIC8vIC0tLS0tLSBHRU5FUkFMLUNPTlRFTlQgQ09NUE9ORU5UUzpcbiAgICAgICAgTWFpbkhlYWRlckljb25Db21wb25lbnQsXG4gICAgICAgIENvbnRlbnRIZWFkZXJJdGVtQ29tcG9uZW50LFxuICAgICAgICBDb250ZW50RGl2aWRlckNvbXBvbmVudCxcbiAgICAgICAgQ3VzdG9tTW9kYWxDb21wb25lbnQsXG4gICAgICAgIENvdW50cnlQaWNrZXJDb21wb25lbnQsXG4gICAgICAgIExhbmd1YWdlUGlja2VyQ29tcG9uZW50LFxuICAgICAgICBMb2NhdGlvblBpY2tlckNvbXBvbmVudCxcblxuICAgICAgICAvLyAtLS0tLS0gQlVTSU5FU1MgQ09NUE9ORU5UUzpcbiAgICAgICAgQ2FyZENvbmZpZ3VyYXRpb25Gb3JtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBFVkVOVFMgQ09NUE9ORU5UUzpcbiAgICAgICAgQXBwb2ludG1lbnRJdGVtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBNRVNTQUdFUyBDT01QT05FTlRTOlxuICAgICAgICBDb252ZXJzYXRpb25JdGVtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBPTkJPQVJESU5HIENPTVBPTkVOVFM6XG4gICAgICAgIEhpZGRlbklucHV0SXRlbUNvbXBvbmVudCxcbiAgICAgICAgTW9kdWxlUHJvZ3Jlc3NJdGVtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBQQU5FTCBDT01QT05FTlRTOlxuICAgICAgICBLcGlJdGVtQ29tcG9uZW50LFxuICAgICAgICBXaWRnZXRIZWFkZXJJdGVtQ29tcG9uZW50LFxuXG4gICAgICAgIC8vIC0tLS0tLSBQQU5FTCBDT01QT05FTlRTOlxuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgU2hhcmVkQ29tcG9uZW50c01vZHVsZSB7IH1cbiJdfQ==