import { __decorate, __awaiter } from 'tslib';
import { CommonModule } from '@angular/common';
import { Component, Input, ViewChild, EventEmitter, Output, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalController, IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Subject } from 'rxjs';

let CardConfigurationFormComponent = class CardConfigurationFormComponent {
    constructor() {
        this.country = { name: '', flag: '' };
    }
    ngOnInit() { }
    openCountryPicker() { }
};
CardConfigurationFormComponent = __decorate([
    Component({
        selector: 'boxx-card-configuration-form',
        template: "<div class=\"main-wrapper\">\n\n    <ion-list class=\"boxx-input-list\">\n        <ion-row class=\"item-section pos-1\">\n            <ion-col size=\"12\" class=\"ion-no-padding\">\n                <p>N\u00FAmero de tarjeta *</p>\n                <ion-item mode=\"ios\">\n                    <ion-label position=\"stacked\"></ion-label>\n                    <ion-input type=\"number\" placeholder=\"\"></ion-input>\n                    <ion-icon slot=\"start\" src=\"/assets/icon/Icon_SubMenuProfileAccountPayment.svg\"></ion-icon>\n                    <ion-icon slot=\"end\" src=\"/assets/icon/Btn_MessengerCamera.svg\"></ion-icon>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n\n        <ion-item mode=\"ios\" class=\"item-section pos-2\">\n            <ion-label position=\"stacked\">Alias *</ion-label>\n            <ion-input placeholder=\"Alias de tu tarjeta\"></ion-input>\n        </ion-item>\n\n        <ion-row class=\"item-section pos-3\">\n            <ion-col size=\"6\" class=\"ion-no-padding\">\n                <p>Fecha de expiraci\u00F3n *</p>\n                <ion-item mode=\"ios\">\n                    <ion-label></ion-label>\n                    <ion-input placeholder=\"MM/YY\"></ion-input>\n                </ion-item>\n            </ion-col>\n            <ion-col size=\"6\" class=\"ion-no-padding\">\n                <p>CVV *</p>\n                <ion-item mode=\"ios\">\n                    <ion-label></ion-label>\n                    <ion-input placeholder=\"CVV\"></ion-input>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n\n        <ion-item mode=\"ios\" class=\"item-section pos-4\" (click)=\"openCountryPicker()\">\n            <ion-label position=\"stacked\">Pa\u00EDs *</ion-label>\n            <ion-input [hidden]=\"country.name\" readonly placeholder=\"Seleccionar Pa\u00EDs\">\n            </ion-input>\n            <div class=\"input-with-right-icon\" [hidden]=\"!country.name\">\n                <div>{{ country.name }}</div>\n                <ion-img [src]=\"country.flag\"></ion-img>\n            </div>\n            <ion-icon slot=\"end\" size=\"small\" name=\"caret-down\" color=\"primary\"></ion-icon>\n        </ion-item>\n\n        <ion-item mode=\"ios\" class=\"item-section pos-5\">\n            <ion-label position=\"stacked\">C.P *</ion-label>\n            <ion-input placeholder=\"C\u00F3digo postal\"></ion-input>\n        </ion-item>\n\n    </ion-list>\n\n    <p class=\"ion-text-center\">\n        <ion-text color=\"danger\">Tarjeta invalida</ion-text>\n    </p>\n</div>",
        styles: ["ion-list{background:var(--boxx-list-background,unset)}ion-item{--border-width:0;margin-bottom:24px;--min-height:64px}#card-number-item ion-icon[slot=start]{margin-left:16px;margin-right:16px}#card-number-item ion-icon[slot=end]{margin-right:16px}#card-expiration-date ion-input{min-width:5vw;max-width:12vw;background:var(--boxx-expiration-input-background,var(--boxx-app-background,#fff));padding-left:var(--boxx-expiration-input-padding-left,8px)!important;padding-right:var(--boxx-expiration-input-padding-right,8px)!important}#card-expiration-date ion-input:first-of-type{margin-right:var(--boxx-expiration-leftinput-margin-right,4px)}#card-expiration-date ion-input:last-of-type{margin-left:var(--boxx-expiration-rightinput-margin-left,4px)}ion-col{padding:0}.pos-3 ion-col:first-child,ion-col:first-child{padding-right:8px}ion-col:last-child{padding-left:8px}p{font-size:14px;font-weight:600;padding-left:16px;padding-right:16px;margin-left:16px;margin-bottom:16px}.pos-3 ion-col:last-child{padding-left:8px}"]
    })
], CardConfigurationFormComponent);

let AppointmentItemComponent = class AppointmentItemComponent {
    constructor() { }
    ngOnInit() { }
};
AppointmentItemComponent = __decorate([
    Component({
        selector: 'boxx-appointment-item',
        template: "<p>\n  appointment-item works!\n</p>\n",
        styles: [""]
    })
], AppointmentItemComponent);

let ContentDividerComponent = class ContentDividerComponent {
    constructor() { }
    ngOnInit() { }
};
ContentDividerComponent = __decorate([
    Component({
        selector: 'boxx-content-divider',
        template: "<div class=\"icon-wrapper\">\n    <ion-icon class=\"horizontal-divider\" src=\"assets/icon/Ornament_HomeSeparator.svg\"></ion-icon>\n</div>",
        styles: [".icon-wrapper{height:var(--boxx-content-divider-height,4px);margin-top:var(--boxx-content-divider-margin-top,22px);margin-bottom:var(--boxx-content-divider-margin-bottom,22px);margin-left:var(--boxx-content-divider-margin-left,16px);margin-right:var(--boxx-content-divider-margin-right,16px);padding:var(--boxx-content-divider-padding,0);line-height:var(--boxx-content-line-height,0)}.icon-wrapper ion-icon{width:var(--boxx-icon-width,100%);height:var(--boxx-icon-height,4px)}:host([position=vertical])>div{border:1px solid #fff;transform:rotate(90deg);transform-origin:left;position:relative;width:100%}"]
    })
], ContentDividerComponent);

let ContentHeaderItemComponent = class ContentHeaderItemComponent {
    constructor() { }
    ngOnInit() { }
};
ContentHeaderItemComponent = __decorate([
    Component({
        selector: 'boxx-content-header-item',
        template: "<div class=\"content-header-wrapper\">\n    <div class=\"icon\">\n        <ng-content select=\"ion-icon\"></ng-content>\n    </div>\n    <div class=\"title\">\n        <ng-content></ng-content>\n    </div>\n</div>",
        styles: [".content-header-wrapper{padding-top:var(--boxx-header-padding-top,0);padding-bottom:var(--boxx-header-padding-bottom,0);background:var(--boxx-header-background,transparent)}.content-header-wrapper .icon{margin-top:var(--boxx-header-icon-margin-top,8px);margin-bottom:var(--boxx-header-icon-margin-bottom,24px);line-height:var(--boxx-header-icon-line-height,0);text-align:var(--boxx-header-icon-text-align,center)}.content-header-wrapper .icon ::ng-deep ion-icon{color:var(--boxx-header-icon-color,var(--ion-color-primary-contrast));width:var(--boxx-header-icon-size,26px);height:var(--boxx-header-icon-size,26px)}.content-header-wrapper .title{margin-top:var(--boxx-header-title-margin-top,24px);margin-bottom:var(--boxx-header-title-margin-bottom,16px);line-height:var(--boxx-header-title-line-height,inherit);text-align:var(--boxx-header-title-text-align,center);font-size:var(--boxx-header-title-font-size,21px);font-weight:var(--boxx-header-title-font-weight,700);color:var(--boxx-header-title-color,var(--ion-color-primary-contrast))}"]
    })
], ContentHeaderItemComponent);

let CountryPickerComponent = class CountryPickerComponent {
    constructor(viewController) {
        this.viewController = viewController;
        this.countries = [];
        this.items = [];
    }
    ngOnInit() {
        this.items = this.countries;
    }
    select(country) {
        this.viewController.dismiss(country);
    }
    search(searchTerm) {
        this.items = this.countries.filter(country => (country.translations.es || country.name || '')
            .toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') // replace accented chars
            .indexOf(searchTerm.toLowerCase()) > -1);
    }
    close() {
        this.viewController.dismiss();
    }
};
CountryPickerComponent.ctorParameters = () => [
    { type: ModalController }
];
__decorate([
    Input()
], CountryPickerComponent.prototype, "countries", void 0);
CountryPickerComponent = __decorate([
    Component({
        selector: 'boxx-country-selector',
        template: "<boxx-custom-modal>\n    <div title translate>GENERAL.ACTION.selectCountry</div>\n\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #CountryPicker showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n            (ionChange)=\"search($event.detail.value)\" placeholder=\"{{'GENERAL.ACTION.searchCountry' | translate}}\">\n        </ion-searchbar>\n    </ion-item-divider>\n\n    <ion-virtual-scroll [items]=\"items\" approxItemHeight=\"64px\">\n        <ion-item *virtualItem=\"let country\" (click)=\"select(country)\">\n            <ion-label>\n                <ion-img [src]=\"country.flag\" [alt]=\"country.name\" style=\"width: 24px; display: inline-block;\">\n                </ion-img>\n                {{country.translations.es || country.name}}\n            </ion-label>\n        </ion-item>\n    </ion-virtual-scroll>\n</boxx-custom-modal>",
        styles: [""]
    })
], CountryPickerComponent);

let CustomModalComponent = class CustomModalComponent {
    constructor(popoverController) {
        this.popoverController = popoverController;
    }
    ngOnInit() { }
    close() {
        this.popoverController.dismiss();
    }
};
CustomModalComponent.ctorParameters = () => [
    { type: ModalController }
];
CustomModalComponent = __decorate([
    Component({
        selector: 'boxx-custom-modal',
        template: "<ion-header class=\"ion-no-border\">\n    <ion-toolbar>\n        <div id=\"grabber\"></div>\n        <ion-title>\n            <ng-content select=\"div[title]\"></ng-content>\n        </ion-title>\n\n        <ion-buttons slot=\"end\">\n            <ion-button (click)=\"close()\">\n                <ion-icon slot=\"icon-only\" name=\"close-circle\" color=\"medium\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ng-content></ng-content>\n</ion-content>",
        styles: [":host{background:var(--boxx-modal-header-background,#01091a)}ion-header ion-toolbar{margin-top:var(--boxx-modal-toolbar-margin-top,45px);margin-bottom:var(--boxx-modal-toolbar-margin-bottom,-2px);border-top-left-radius:var(--boxx-modal-toolbar-topleft-radius,10px);border-top-right-radius:var(--boxx-modal-toolbar-topright-radius,10px);--background:var(--boxx-modal-toolbar-background, var(--boxx-app-background, white));color:var(--boxx-modal-toolbar-color,var(--ion-color-dark))}ion-header ion-toolbar ion-title{margin-left:var(--boxx-modal-title-margin-left,11px)!important;font-size:var(--boxx-modal-title-font-size,17px);font-weight:var(--boxx-modal-title-font-weight,700)}ion-header ion-toolbar ion-buttons[slot=end]{margin-right:var(--boxx-modal-buttons-end-margin-right,4px)}ion-header ion-toolbar #grabber{width:var(--boxx-modal-grabber-width,36px);height:var(--boxx-modal-grabber-height,5px);background:var(--boxx-modal-grabber-background,#c7c7cc);margin:var(--boxx-modal-grabber-margin,0 auto);position:var(--boxx-modal-grabber-position,absolute);top:var(--boxx-modal-grabber-top,7px);left:var(--boxx-modal-grabber-left,15px);right:var(--boxx-modal-grabber-right,0)}ion-content{position:var(--boxx-modal-content-position,absolute);height:var(--boxx-modal-content-height,100%)}"]
    })
], CustomModalComponent);

let LanguagePickerComponent = class LanguagePickerComponent {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.languageList = [];
    }
    ngOnInit() {
        this.languageList = [
            {
                id: 'en',
                name: 'English',
                description: 'English (US)',
                selected: false,
            },
            {
                id: 'es',
                name: 'Spanish',
                description: 'Spanish (Latin America)',
                selected: false,
            },
            {
                id: 'pt',
                name: 'Portuguese',
                description: 'Portugués',
                selected: false,
            },
        ];
        this.languageList.some(language => {
            if (language.id === this.selectedId) {
                language.selected = true;
                return true;
            }
        });
    }
    closeView() {
        this.modalCtrl.dismiss();
    }
    selectLanguage(language) {
        this.modalCtrl.dismiss(language);
    }
};
LanguagePickerComponent.ctorParameters = () => [
    { type: ModalController }
];
__decorate([
    Input()
], LanguagePickerComponent.prototype, "languageList", void 0);
__decorate([
    Input()
], LanguagePickerComponent.prototype, "selectedId", void 0);
LanguagePickerComponent = __decorate([
    Component({
        selector: 'app-language-picker',
        template: "<boxx-custom-modal>\n    <div title translate>Idioma - Language</div>\n    <ion-list>\n        <ion-item mode=\"ios\" *ngFor=\"let language of languageList\" (click)=\"selectLanguage(language)\">\n            <ion-label position=\"stacked\">language.name</ion-label>\n            <ion-input readonly [value]=\"language.description\"></ion-input>\n            <ion-icon *ngIf=\"language.selected\" slot=\"end\" size=\"small\" color=\"success\"\n                src=\"assets/icon/Icon_NoActiveModule_Step.svg\"></ion-icon>\n        </ion-item>\n    </ion-list>\n</boxx-custom-modal>",
        styles: [""]
    })
], LanguagePickerComponent);

let LocationPickerComponent = class LocationPickerComponent {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.destroyed$ = new Subject();
        this.searchResults = [];
        this.mapsService = new google.maps.places.AutocompleteService();
    }
    ngOnDestroy() {
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }
    ionViewDidEnter() {
        setTimeout(() => {
            this.searchBar.setFocus();
        }, 200);
    }
    onInput(query) {
        this.searchResults = [];
        if (!query) {
            return;
        }
        const config = {
            types: [],
            input: query
        };
        this.mapsService.getPlacePredictions(config, (predictions, status) => {
            if (predictions && predictions.length > 0) {
                predictions.forEach((prediction) => {
                    this.searchResults.push(prediction);
                });
            }
            else {
                const locationDesc = {
                    description: query,
                    place_id: 'default_place_id'
                };
                this.searchResults.push(locationDesc);
            }
        });
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            this.modalCtrl.dismiss( /*{someData: ...} */);
        });
    }
    chooseItem(item) {
        this.modalCtrl.dismiss({ description: item.description, id: item.place_id });
    }
    select() { }
};
LocationPickerComponent.ctorParameters = () => [
    { type: ModalController }
];
__decorate([
    ViewChild('placePickerSB', { static: false })
], LocationPickerComponent.prototype, "searchBar", void 0);
LocationPickerComponent = __decorate([
    Component({
        selector: 'boxx-location-picker',
        template: "<boxx-custom-modal>\n    <div title translate>FORM.LABEL.place</div>\n\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #placePickerSB debounce=\"500\" showCancelButton=\"never\"\n            (ionInput)=\"onInput($event.target.value)\" cancelButtonIcon=\"close-circle-outline\"\n            placeholder='{{\"EVENTS.enterAddress\" | translate}}'></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-list>\n        <ion-item mode=\"ios\" *ngFor=\"let item of searchResults\" (click)=\"chooseItem(item)\">\n            <ion-label class=\"ion-text-wrap\">{{ item.description }}</ion-label>\n        </ion-item>\n    </ion-list>\n</boxx-custom-modal>",
        styles: [""]
    })
], LocationPickerComponent);

let MainHeaderIconComponent = class MainHeaderIconComponent {
    constructor() { }
    ngOnInit() { }
};
MainHeaderIconComponent = __decorate([
    Component({
        selector: 'boxx-header-icon',
        template: "<div class=\"brand-icon-wrapper\">\n    <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n</div>",
        styles: [".brand-icon-wrapper{position:var(--boxx-brand-icon-wrapper-position,absolute);top:var(--boxx-brand-icon-wrapper-top,0);width:var(--boxx-brand-icon-wrapper-width,100%);text-align:var(--boxx-brand-icon-wrapper-align,center);z-index:var(--boxx-brand-icon-wrapper-z-index,999);line-height:0}.brand-icon-wrapper ion-icon{width:var(--boxx-brand-icon-width,56px);height:var(--boxx-brand-icon-height,20px);color:var(--boxx-brand-icon-color,#fff)}"]
    })
], MainHeaderIconComponent);

let ConversationItemComponent = class ConversationItemComponent {
    constructor() { }
    ngOnInit() { }
};
ConversationItemComponent = __decorate([
    Component({
        selector: 'boxx-conversation-item',
        template: "<p>\n  conversation-item works!\n</p>\n",
        styles: [""]
    })
], ConversationItemComponent);

let HiddenInputItemComponent = class HiddenInputItemComponent {
    constructor() {
        this.whenInputVisibilityChange = new EventEmitter();
        this.inputShown = false;
    }
    ngOnInit() { }
    toggleRealInputVisibility(show) {
        this.inputShown = show;
        this.whenInputVisibilityChange.emit(show);
    }
};
__decorate([
    Input()
], HiddenInputItemComponent.prototype, "text", void 0);
__decorate([
    Output()
], HiddenInputItemComponent.prototype, "closeIconName", void 0);
__decorate([
    Output()
], HiddenInputItemComponent.prototype, "closeIconSize", void 0);
__decorate([
    Output()
], HiddenInputItemComponent.prototype, "closeIconSlot", void 0);
__decorate([
    Output()
], HiddenInputItemComponent.prototype, "whenInputVisibilityChange", void 0);
HiddenInputItemComponent = __decorate([
    Component({
        selector: 'boxx-hidden-input-item',
        template: "<div class=\"main-input-wrapper\">\n    <ion-item [hidden]=\"inputShown\" class=\"fake-input boxx-bg-transparent\" lines=\"none\">\n        <ion-label (click)=\"toggleRealInputVisibility(true)\">\n            {{text}}\n        </ion-label>\n    </ion-item>\n    <ion-item class=\"real-input boxx-bg-transparent\" [hidden]=\"!inputShown\" lines=\"none\">\n        <ng-content select=\"ion-input\"></ng-content>\n        <ng-content select=\"ion-input | ion-textarea\"></ng-content>\n        <ion-icon class=\"boxx-hidden-input-close-icon\" [name]=\"closeIconName || 'close-circle'\"\n            [size]=\"closeIconSize || 'large'\" (click)=\"toggleRealInputVisibility(false)\"\n            [slot]=\"closeIconSlot || 'end'\">\n        </ion-icon>\n    </ion-item>\n</div>",
        styles: [".fake-input{font-size:21px;font-weight:700;color:var(--fakeinput-icon-color,var(--ion-color-secondary));text-align:var(--fakeinput-align,center);color:var(--fakeinput-color,var(--ion-color-secondary))}.real-input ::ng-deep ion-input,.real-input ::ng-deep ion-textarea{font-size:21px;font-weight:700;color:var(--realinput-color,var(--ion-color-secondary));text-align:var(--realinput-align,center)}.real-input ion-icon#boxx-hidden-input-close-icon{width:29px!important;height:29px!important;margin-top:var(--realinput-icon-margin-top,19px);margin-bottom:var(--realinput-icon-margin-bottom,18px);color:var(--realinput-icon-color,var(--ion-color-secondary))}ion-item{--padding-start:24px;--padding-end:24px;--inner-padding-end:var(--item-inner-padding-end, 0)}"]
    })
], HiddenInputItemComponent);

let ModuleProgressItemComponent = class ModuleProgressItemComponent {
    constructor() {
        this.value = 0; // progress value, from 0 to 1
    }
    ngOnInit() { }
};
__decorate([
    Input()
], ModuleProgressItemComponent.prototype, "text", void 0);
__decorate([
    Input()
], ModuleProgressItemComponent.prototype, "class", void 0);
__decorate([
    Input()
], ModuleProgressItemComponent.prototype, "progressbarColor", void 0);
__decorate([
    Input()
], ModuleProgressItemComponent.prototype, "value", void 0);
ModuleProgressItemComponent = __decorate([
    Component({
        selector: 'boxx-module-progress-item',
        template: "<div class=\"main-wrapper\" [ngClass]=\"class\">\n    <div class=\"progress-wrapper\">\n        <div class=\"text\">{{text}}</div>\n        <ion-progress-bar [color]=\"progressbarColor || 'success'\" [value]=\"value\"></ion-progress-bar>\n    </div>\n    <ng-content></ng-content>\n</div>",
        styles: [".main-wrapper{margin-top:var(--boxx-progress-container-margin-top,24px);margin-bottom:var(--boxx-progress-container-margin-bottom,32px);margin-left:var(--boxx-progress-container-margin-left,16px);margin-right:var(--boxx-progress-container-margin-right,16px);color:var(--boxx-progress-container-color,var(--ion-color-primary));text-align:var(--boxx-progress-container-align,center);font-size:15px;font-weight:600}.main-wrapper .progress-wrapper{color:var(--boxx-progress-text-color,#fff);text-align:var(--boxx-progress-text-align,right);font-size:12px;font-weight:500}.main-wrapper .progress-wrapper .text{padding:var(--boxx-progress-text-padding,0);margin:var(--boxx-progress-text-margin,0 0 4px 0)}.main-wrapper .progress-wrapper ion-progress-bar{height:var(--boxx-progress-bar-height,8px);background:var(--boxx-progress-bar-background,#fff)}.main-wrapper ::ng-deep ion-button{margin:var(--boxx-progress-button-margin,16px 0)}.main-wrapper ::ng-deep ion-button:last-of-type{margin-bottom:32px}"]
    })
], ModuleProgressItemComponent);

let KpiItemComponent = class KpiItemComponent {
    constructor() { }
    ngOnInit() { }
};
KpiItemComponent = __decorate([
    Component({
        selector: 'boxx-kpi-item',
        template: "<p>\n  kpi-item works!\n</p>\n",
        styles: [""]
    })
], KpiItemComponent);

let WidgetHeaderItemComponent = class WidgetHeaderItemComponent {
    constructor() { }
    ngOnInit() { }
};
WidgetHeaderItemComponent = __decorate([
    Component({
        selector: 'boxx-widget-header-item',
        template: "<p>\n  widget-header-item works!\n</p>\n",
        styles: [""]
    })
], WidgetHeaderItemComponent);

let SharedComponentsModule = class SharedComponentsModule {
};
SharedComponentsModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            IonicModule,
            TranslateModule.forChild()
        ],
        declarations: [
            // ------ GENERAL-CONTENT COMPONENTS:
            MainHeaderIconComponent,
            ContentHeaderItemComponent,
            ContentDividerComponent,
            CustomModalComponent,
            CountryPickerComponent,
            LanguagePickerComponent,
            LocationPickerComponent,
            // ------ BUSINESS COMPONENTS:
            CardConfigurationFormComponent,
            // ------ EVENTS COMPONENTS:
            AppointmentItemComponent,
            // ------ MESSAGES COMPONENTS:
            ConversationItemComponent,
            // ------ ONBOARDING COMPONENTS:
            HiddenInputItemComponent,
            ModuleProgressItemComponent,
            // ------ ONBOARDING COMPONENTS:
            KpiItemComponent,
            WidgetHeaderItemComponent,
        ],
        exports: [
            // ------ GENERAL-CONTENT COMPONENTS:
            MainHeaderIconComponent,
            ContentHeaderItemComponent,
            ContentDividerComponent,
            CustomModalComponent,
            CountryPickerComponent,
            LanguagePickerComponent,
            LocationPickerComponent,
            // ------ BUSINESS COMPONENTS:
            CardConfigurationFormComponent,
            // ------ EVENTS COMPONENTS:
            AppointmentItemComponent,
            // ------ MESSAGES COMPONENTS:
            ConversationItemComponent,
            // ------ ONBOARDING COMPONENTS:
            HiddenInputItemComponent,
            ModuleProgressItemComponent,
            // ------ PANEL COMPONENTS:
            KpiItemComponent,
            WidgetHeaderItemComponent,
        ]
    })
], SharedComponentsModule);

/*
 * Public API Surface of shared-components
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CardConfigurationFormComponent, CountryPickerComponent, LanguagePickerComponent, LocationPickerComponent, SharedComponentsModule, MainHeaderIconComponent as ɵa, ContentHeaderItemComponent as ɵb, ContentDividerComponent as ɵc, CustomModalComponent as ɵd, AppointmentItemComponent as ɵe, ConversationItemComponent as ɵf, HiddenInputItemComponent as ɵg, ModuleProgressItemComponent as ɵh, KpiItemComponent as ɵi, WidgetHeaderItemComponent as ɵj };
//# sourceMappingURL=boxx-shared-components.js.map
