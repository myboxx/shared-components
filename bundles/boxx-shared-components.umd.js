(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/core'), require('@angular/forms'), require('@ionic/angular'), require('@ngx-translate/core'), require('rxjs')) :
    typeof define === 'function' && define.amd ? define('@boxx/shared-components', ['exports', '@angular/common', '@angular/core', '@angular/forms', '@ionic/angular', '@ngx-translate/core', 'rxjs'], factory) :
    (global = global || self, factory((global.boxx = global.boxx || {}, global.boxx['shared-components'] = {}), global.ng.common, global.ng.core, global.ng.forms, global['@ionic/angular'], global['@ngx-translate/core'], global.rxjs));
}(this, (function (exports, common, core, forms, angular, core$1, rxjs) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var CardConfigurationFormComponent = /** @class */ (function () {
        function CardConfigurationFormComponent() {
            this.country = { name: '', flag: '' };
        }
        CardConfigurationFormComponent.prototype.ngOnInit = function () { };
        CardConfigurationFormComponent.prototype.openCountryPicker = function () { };
        CardConfigurationFormComponent = __decorate([
            core.Component({
                selector: 'boxx-card-configuration-form',
                template: "<div class=\"main-wrapper\">\n\n    <ion-list class=\"boxx-input-list\">\n        <ion-row class=\"item-section pos-1\">\n            <ion-col size=\"12\" class=\"ion-no-padding\">\n                <p>N\u00FAmero de tarjeta *</p>\n                <ion-item mode=\"ios\">\n                    <ion-label position=\"stacked\"></ion-label>\n                    <ion-input type=\"number\" placeholder=\"\"></ion-input>\n                    <ion-icon slot=\"start\" src=\"/assets/icon/Icon_SubMenuProfileAccountPayment.svg\"></ion-icon>\n                    <ion-icon slot=\"end\" src=\"/assets/icon/Btn_MessengerCamera.svg\"></ion-icon>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n\n        <ion-item mode=\"ios\" class=\"item-section pos-2\">\n            <ion-label position=\"stacked\">Alias *</ion-label>\n            <ion-input placeholder=\"Alias de tu tarjeta\"></ion-input>\n        </ion-item>\n\n        <ion-row class=\"item-section pos-3\">\n            <ion-col size=\"6\" class=\"ion-no-padding\">\n                <p>Fecha de expiraci\u00F3n *</p>\n                <ion-item mode=\"ios\">\n                    <ion-label></ion-label>\n                    <ion-input placeholder=\"MM/YY\"></ion-input>\n                </ion-item>\n            </ion-col>\n            <ion-col size=\"6\" class=\"ion-no-padding\">\n                <p>CVV *</p>\n                <ion-item mode=\"ios\">\n                    <ion-label></ion-label>\n                    <ion-input placeholder=\"CVV\"></ion-input>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n\n        <ion-item mode=\"ios\" class=\"item-section pos-4\" (click)=\"openCountryPicker()\">\n            <ion-label position=\"stacked\">Pa\u00EDs *</ion-label>\n            <ion-input [hidden]=\"country.name\" readonly placeholder=\"Seleccionar Pa\u00EDs\">\n            </ion-input>\n            <div class=\"input-with-right-icon\" [hidden]=\"!country.name\">\n                <div>{{ country.name }}</div>\n                <ion-img [src]=\"country.flag\"></ion-img>\n            </div>\n            <ion-icon slot=\"end\" size=\"small\" name=\"caret-down\" color=\"primary\"></ion-icon>\n        </ion-item>\n\n        <ion-item mode=\"ios\" class=\"item-section pos-5\">\n            <ion-label position=\"stacked\">C.P *</ion-label>\n            <ion-input placeholder=\"C\u00F3digo postal\"></ion-input>\n        </ion-item>\n\n    </ion-list>\n\n    <p class=\"ion-text-center\">\n        <ion-text color=\"danger\">Tarjeta invalida</ion-text>\n    </p>\n</div>",
                styles: ["ion-list{background:var(--boxx-list-background,unset)}ion-item{--border-width:0;margin-bottom:24px;--min-height:64px}#card-number-item ion-icon[slot=start]{margin-left:16px;margin-right:16px}#card-number-item ion-icon[slot=end]{margin-right:16px}#card-expiration-date ion-input{min-width:5vw;max-width:12vw;background:var(--boxx-expiration-input-background,var(--boxx-app-background,#fff));padding-left:var(--boxx-expiration-input-padding-left,8px)!important;padding-right:var(--boxx-expiration-input-padding-right,8px)!important}#card-expiration-date ion-input:first-of-type{margin-right:var(--boxx-expiration-leftinput-margin-right,4px)}#card-expiration-date ion-input:last-of-type{margin-left:var(--boxx-expiration-rightinput-margin-left,4px)}ion-col{padding:0}.pos-3 ion-col:first-child,ion-col:first-child{padding-right:8px}ion-col:last-child{padding-left:8px}p{font-size:14px;font-weight:600;padding-left:16px;padding-right:16px;margin-left:16px;margin-bottom:16px}.pos-3 ion-col:last-child{padding-left:8px}"]
            })
        ], CardConfigurationFormComponent);
        return CardConfigurationFormComponent;
    }());

    var AppointmentItemComponent = /** @class */ (function () {
        function AppointmentItemComponent() {
        }
        AppointmentItemComponent.prototype.ngOnInit = function () { };
        AppointmentItemComponent = __decorate([
            core.Component({
                selector: 'boxx-appointment-item',
                template: "<p>\n  appointment-item works!\n</p>\n",
                styles: [""]
            })
        ], AppointmentItemComponent);
        return AppointmentItemComponent;
    }());

    var ContentDividerComponent = /** @class */ (function () {
        function ContentDividerComponent() {
        }
        ContentDividerComponent.prototype.ngOnInit = function () { };
        ContentDividerComponent = __decorate([
            core.Component({
                selector: 'boxx-content-divider',
                template: "<div class=\"icon-wrapper\">\n    <ion-icon class=\"horizontal-divider\" src=\"assets/icon/Ornament_HomeSeparator.svg\"></ion-icon>\n</div>",
                styles: [".icon-wrapper{height:var(--boxx-content-divider-height,4px);margin-top:var(--boxx-content-divider-margin-top,22px);margin-bottom:var(--boxx-content-divider-margin-bottom,22px);margin-left:var(--boxx-content-divider-margin-left,16px);margin-right:var(--boxx-content-divider-margin-right,16px);padding:var(--boxx-content-divider-padding,0);line-height:var(--boxx-content-line-height,0)}.icon-wrapper ion-icon{width:var(--boxx-icon-width,100%);height:var(--boxx-icon-height,4px)}:host([position=vertical])>div{border:1px solid #fff;transform:rotate(90deg);transform-origin:left;position:relative;width:100%}"]
            })
        ], ContentDividerComponent);
        return ContentDividerComponent;
    }());

    var ContentHeaderItemComponent = /** @class */ (function () {
        function ContentHeaderItemComponent() {
        }
        ContentHeaderItemComponent.prototype.ngOnInit = function () { };
        ContentHeaderItemComponent = __decorate([
            core.Component({
                selector: 'boxx-content-header-item',
                template: "<div class=\"content-header-wrapper\">\n    <div class=\"icon\">\n        <ng-content select=\"ion-icon\"></ng-content>\n    </div>\n    <div class=\"title\">\n        <ng-content></ng-content>\n    </div>\n</div>",
                styles: [".content-header-wrapper{padding-top:var(--boxx-header-padding-top,0);padding-bottom:var(--boxx-header-padding-bottom,0);background:var(--boxx-header-background,transparent)}.content-header-wrapper .icon{margin-top:var(--boxx-header-icon-margin-top,8px);margin-bottom:var(--boxx-header-icon-margin-bottom,24px);line-height:var(--boxx-header-icon-line-height,0);text-align:var(--boxx-header-icon-text-align,center)}.content-header-wrapper .icon ::ng-deep ion-icon{color:var(--boxx-header-icon-color,var(--ion-color-primary-contrast));width:var(--boxx-header-icon-size,26px);height:var(--boxx-header-icon-size,26px)}.content-header-wrapper .title{margin-top:var(--boxx-header-title-margin-top,24px);margin-bottom:var(--boxx-header-title-margin-bottom,16px);line-height:var(--boxx-header-title-line-height,inherit);text-align:var(--boxx-header-title-text-align,center);font-size:var(--boxx-header-title-font-size,21px);font-weight:var(--boxx-header-title-font-weight,700);color:var(--boxx-header-title-color,var(--ion-color-primary-contrast))}"]
            })
        ], ContentHeaderItemComponent);
        return ContentHeaderItemComponent;
    }());

    var CountryPickerComponent = /** @class */ (function () {
        function CountryPickerComponent(viewController) {
            this.viewController = viewController;
            this.countries = [];
            this.items = [];
        }
        CountryPickerComponent.prototype.ngOnInit = function () {
            this.items = this.countries;
        };
        CountryPickerComponent.prototype.select = function (country) {
            this.viewController.dismiss(country);
        };
        CountryPickerComponent.prototype.search = function (searchTerm) {
            this.items = this.countries.filter(function (country) {
                return (country.translations.es || country.name || '')
                    .toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') // replace accented chars
                    .indexOf(searchTerm.toLowerCase()) > -1;
            });
        };
        CountryPickerComponent.prototype.close = function () {
            this.viewController.dismiss();
        };
        CountryPickerComponent.ctorParameters = function () { return [
            { type: angular.ModalController }
        ]; };
        __decorate([
            core.Input()
        ], CountryPickerComponent.prototype, "countries", void 0);
        CountryPickerComponent = __decorate([
            core.Component({
                selector: 'boxx-country-selector',
                template: "<boxx-custom-modal>\n    <div title translate>GENERAL.ACTION.selectCountry</div>\n\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #CountryPicker showCancelButton=\"never\" cancelButtonIcon=\"close-circle-outline\"\n            (ionChange)=\"search($event.detail.value)\" placeholder=\"{{'GENERAL.ACTION.searchCountry' | translate}}\">\n        </ion-searchbar>\n    </ion-item-divider>\n\n    <ion-virtual-scroll [items]=\"items\" approxItemHeight=\"64px\">\n        <ion-item *virtualItem=\"let country\" (click)=\"select(country)\">\n            <ion-label>\n                <ion-img [src]=\"country.flag\" [alt]=\"country.name\" style=\"width: 24px; display: inline-block;\">\n                </ion-img>\n                {{country.translations.es || country.name}}\n            </ion-label>\n        </ion-item>\n    </ion-virtual-scroll>\n</boxx-custom-modal>",
                styles: [""]
            })
        ], CountryPickerComponent);
        return CountryPickerComponent;
    }());

    var CustomModalComponent = /** @class */ (function () {
        function CustomModalComponent(popoverController) {
            this.popoverController = popoverController;
        }
        CustomModalComponent.prototype.ngOnInit = function () { };
        CustomModalComponent.prototype.close = function () {
            this.popoverController.dismiss();
        };
        CustomModalComponent.ctorParameters = function () { return [
            { type: angular.ModalController }
        ]; };
        CustomModalComponent = __decorate([
            core.Component({
                selector: 'boxx-custom-modal',
                template: "<ion-header class=\"ion-no-border\">\n    <ion-toolbar>\n        <div id=\"grabber\"></div>\n        <ion-title>\n            <ng-content select=\"div[title]\"></ng-content>\n        </ion-title>\n\n        <ion-buttons slot=\"end\">\n            <ion-button (click)=\"close()\">\n                <ion-icon slot=\"icon-only\" name=\"close-circle\" color=\"medium\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ng-content></ng-content>\n</ion-content>",
                styles: [":host{background:var(--boxx-modal-header-background,#01091a)}ion-header ion-toolbar{margin-top:var(--boxx-modal-toolbar-margin-top,45px);margin-bottom:var(--boxx-modal-toolbar-margin-bottom,-2px);border-top-left-radius:var(--boxx-modal-toolbar-topleft-radius,10px);border-top-right-radius:var(--boxx-modal-toolbar-topright-radius,10px);--background:var(--boxx-modal-toolbar-background, var(--boxx-app-background, white));color:var(--boxx-modal-toolbar-color,var(--ion-color-dark))}ion-header ion-toolbar ion-title{margin-left:var(--boxx-modal-title-margin-left,11px)!important;font-size:var(--boxx-modal-title-font-size,17px);font-weight:var(--boxx-modal-title-font-weight,700)}ion-header ion-toolbar ion-buttons[slot=end]{margin-right:var(--boxx-modal-buttons-end-margin-right,4px)}ion-header ion-toolbar #grabber{width:var(--boxx-modal-grabber-width,36px);height:var(--boxx-modal-grabber-height,5px);background:var(--boxx-modal-grabber-background,#c7c7cc);margin:var(--boxx-modal-grabber-margin,0 auto);position:var(--boxx-modal-grabber-position,absolute);top:var(--boxx-modal-grabber-top,7px);left:var(--boxx-modal-grabber-left,15px);right:var(--boxx-modal-grabber-right,0)}ion-content{position:var(--boxx-modal-content-position,absolute);height:var(--boxx-modal-content-height,100%)}"]
            })
        ], CustomModalComponent);
        return CustomModalComponent;
    }());

    var LanguagePickerComponent = /** @class */ (function () {
        function LanguagePickerComponent(modalCtrl) {
            this.modalCtrl = modalCtrl;
            this.languageList = [];
        }
        LanguagePickerComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.languageList = [
                {
                    id: 'en',
                    name: 'English',
                    description: 'English (US)',
                    selected: false,
                },
                {
                    id: 'es',
                    name: 'Spanish',
                    description: 'Spanish (Latin America)',
                    selected: false,
                },
                {
                    id: 'pt',
                    name: 'Portuguese',
                    description: 'Portugués',
                    selected: false,
                },
            ];
            this.languageList.some(function (language) {
                if (language.id === _this.selectedId) {
                    language.selected = true;
                    return true;
                }
            });
        };
        LanguagePickerComponent.prototype.closeView = function () {
            this.modalCtrl.dismiss();
        };
        LanguagePickerComponent.prototype.selectLanguage = function (language) {
            this.modalCtrl.dismiss(language);
        };
        LanguagePickerComponent.ctorParameters = function () { return [
            { type: angular.ModalController }
        ]; };
        __decorate([
            core.Input()
        ], LanguagePickerComponent.prototype, "languageList", void 0);
        __decorate([
            core.Input()
        ], LanguagePickerComponent.prototype, "selectedId", void 0);
        LanguagePickerComponent = __decorate([
            core.Component({
                selector: 'app-language-picker',
                template: "<boxx-custom-modal>\n    <div title translate>Idioma - Language</div>\n    <ion-list>\n        <ion-item mode=\"ios\" *ngFor=\"let language of languageList\" (click)=\"selectLanguage(language)\">\n            <ion-label position=\"stacked\">language.name</ion-label>\n            <ion-input readonly [value]=\"language.description\"></ion-input>\n            <ion-icon *ngIf=\"language.selected\" slot=\"end\" size=\"small\" color=\"success\"\n                src=\"assets/icon/Icon_NoActiveModule_Step.svg\"></ion-icon>\n        </ion-item>\n    </ion-list>\n</boxx-custom-modal>",
                styles: [""]
            })
        ], LanguagePickerComponent);
        return LanguagePickerComponent;
    }());

    var LocationPickerComponent = /** @class */ (function () {
        function LocationPickerComponent(modalCtrl) {
            this.modalCtrl = modalCtrl;
            this.destroyed$ = new rxjs.Subject();
            this.searchResults = [];
            this.mapsService = new google.maps.places.AutocompleteService();
        }
        LocationPickerComponent.prototype.ngOnDestroy = function () {
            this.destroyed$.next(true);
            this.destroyed$.complete();
        };
        LocationPickerComponent.prototype.ionViewDidEnter = function () {
            var _this = this;
            setTimeout(function () {
                _this.searchBar.setFocus();
            }, 200);
        };
        LocationPickerComponent.prototype.onInput = function (query) {
            var _this = this;
            this.searchResults = [];
            if (!query) {
                return;
            }
            var config = {
                types: [],
                input: query
            };
            this.mapsService.getPlacePredictions(config, function (predictions, status) {
                if (predictions && predictions.length > 0) {
                    predictions.forEach(function (prediction) {
                        _this.searchResults.push(prediction);
                    });
                }
                else {
                    var locationDesc = {
                        description: query,
                        place_id: 'default_place_id'
                    };
                    _this.searchResults.push(locationDesc);
                }
            });
        };
        LocationPickerComponent.prototype.close = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    this.modalCtrl.dismiss( /*{someData: ...} */);
                    return [2 /*return*/];
                });
            });
        };
        LocationPickerComponent.prototype.chooseItem = function (item) {
            this.modalCtrl.dismiss({ description: item.description, id: item.place_id });
        };
        LocationPickerComponent.prototype.select = function () { };
        LocationPickerComponent.ctorParameters = function () { return [
            { type: angular.ModalController }
        ]; };
        __decorate([
            core.ViewChild('placePickerSB', { static: false })
        ], LocationPickerComponent.prototype, "searchBar", void 0);
        LocationPickerComponent = __decorate([
            core.Component({
                selector: 'boxx-location-picker',
                template: "<boxx-custom-modal>\n    <div title translate>FORM.LABEL.place</div>\n\n    <ion-item-divider color=\"light\" sticky>\n        <ion-searchbar mode=\"md\" #placePickerSB debounce=\"500\" showCancelButton=\"never\"\n            (ionInput)=\"onInput($event.target.value)\" cancelButtonIcon=\"close-circle-outline\"\n            placeholder='{{\"EVENTS.enterAddress\" | translate}}'></ion-searchbar>\n    </ion-item-divider>\n\n    <ion-list>\n        <ion-item mode=\"ios\" *ngFor=\"let item of searchResults\" (click)=\"chooseItem(item)\">\n            <ion-label class=\"ion-text-wrap\">{{ item.description }}</ion-label>\n        </ion-item>\n    </ion-list>\n</boxx-custom-modal>",
                styles: [""]
            })
        ], LocationPickerComponent);
        return LocationPickerComponent;
    }());

    var MainHeaderIconComponent = /** @class */ (function () {
        function MainHeaderIconComponent() {
        }
        MainHeaderIconComponent.prototype.ngOnInit = function () { };
        MainHeaderIconComponent = __decorate([
            core.Component({
                selector: 'boxx-header-icon',
                template: "<div class=\"brand-icon-wrapper\">\n    <ion-icon src=\"assets/icon/Icon_Header_Brand.svg\"></ion-icon>\n</div>",
                styles: [".brand-icon-wrapper{position:var(--boxx-brand-icon-wrapper-position,absolute);top:var(--boxx-brand-icon-wrapper-top,0);width:var(--boxx-brand-icon-wrapper-width,100%);text-align:var(--boxx-brand-icon-wrapper-align,center);z-index:var(--boxx-brand-icon-wrapper-z-index,999);line-height:0}.brand-icon-wrapper ion-icon{width:var(--boxx-brand-icon-width,56px);height:var(--boxx-brand-icon-height,20px);color:var(--boxx-brand-icon-color,#fff)}"]
            })
        ], MainHeaderIconComponent);
        return MainHeaderIconComponent;
    }());

    var ConversationItemComponent = /** @class */ (function () {
        function ConversationItemComponent() {
        }
        ConversationItemComponent.prototype.ngOnInit = function () { };
        ConversationItemComponent = __decorate([
            core.Component({
                selector: 'boxx-conversation-item',
                template: "<p>\n  conversation-item works!\n</p>\n",
                styles: [""]
            })
        ], ConversationItemComponent);
        return ConversationItemComponent;
    }());

    var HiddenInputItemComponent = /** @class */ (function () {
        function HiddenInputItemComponent() {
            this.whenInputVisibilityChange = new core.EventEmitter();
            this.inputShown = false;
        }
        HiddenInputItemComponent.prototype.ngOnInit = function () { };
        HiddenInputItemComponent.prototype.toggleRealInputVisibility = function (show) {
            this.inputShown = show;
            this.whenInputVisibilityChange.emit(show);
        };
        __decorate([
            core.Input()
        ], HiddenInputItemComponent.prototype, "text", void 0);
        __decorate([
            core.Output()
        ], HiddenInputItemComponent.prototype, "closeIconName", void 0);
        __decorate([
            core.Output()
        ], HiddenInputItemComponent.prototype, "closeIconSize", void 0);
        __decorate([
            core.Output()
        ], HiddenInputItemComponent.prototype, "closeIconSlot", void 0);
        __decorate([
            core.Output()
        ], HiddenInputItemComponent.prototype, "whenInputVisibilityChange", void 0);
        HiddenInputItemComponent = __decorate([
            core.Component({
                selector: 'boxx-hidden-input-item',
                template: "<div class=\"main-input-wrapper\">\n    <ion-item [hidden]=\"inputShown\" class=\"fake-input boxx-bg-transparent\" lines=\"none\">\n        <ion-label (click)=\"toggleRealInputVisibility(true)\">\n            {{text}}\n        </ion-label>\n    </ion-item>\n    <ion-item class=\"real-input boxx-bg-transparent\" [hidden]=\"!inputShown\" lines=\"none\">\n        <ng-content select=\"ion-input\"></ng-content>\n        <ng-content select=\"ion-input | ion-textarea\"></ng-content>\n        <ion-icon class=\"boxx-hidden-input-close-icon\" [name]=\"closeIconName || 'close-circle'\"\n            [size]=\"closeIconSize || 'large'\" (click)=\"toggleRealInputVisibility(false)\"\n            [slot]=\"closeIconSlot || 'end'\">\n        </ion-icon>\n    </ion-item>\n</div>",
                styles: [".fake-input{font-size:21px;font-weight:700;color:var(--fakeinput-icon-color,var(--ion-color-secondary));text-align:var(--fakeinput-align,center);color:var(--fakeinput-color,var(--ion-color-secondary))}.real-input ::ng-deep ion-input,.real-input ::ng-deep ion-textarea{font-size:21px;font-weight:700;color:var(--realinput-color,var(--ion-color-secondary));text-align:var(--realinput-align,center)}.real-input ion-icon#boxx-hidden-input-close-icon{width:29px!important;height:29px!important;margin-top:var(--realinput-icon-margin-top,19px);margin-bottom:var(--realinput-icon-margin-bottom,18px);color:var(--realinput-icon-color,var(--ion-color-secondary))}ion-item{--padding-start:24px;--padding-end:24px;--inner-padding-end:var(--item-inner-padding-end, 0)}"]
            })
        ], HiddenInputItemComponent);
        return HiddenInputItemComponent;
    }());

    var ModuleProgressItemComponent = /** @class */ (function () {
        function ModuleProgressItemComponent() {
            this.value = 0; // progress value, from 0 to 1
        }
        ModuleProgressItemComponent.prototype.ngOnInit = function () { };
        __decorate([
            core.Input()
        ], ModuleProgressItemComponent.prototype, "text", void 0);
        __decorate([
            core.Input()
        ], ModuleProgressItemComponent.prototype, "class", void 0);
        __decorate([
            core.Input()
        ], ModuleProgressItemComponent.prototype, "progressbarColor", void 0);
        __decorate([
            core.Input()
        ], ModuleProgressItemComponent.prototype, "value", void 0);
        ModuleProgressItemComponent = __decorate([
            core.Component({
                selector: 'boxx-module-progress-item',
                template: "<div class=\"main-wrapper\" [ngClass]=\"class\">\n    <div class=\"progress-wrapper\">\n        <div class=\"text\">{{text}}</div>\n        <ion-progress-bar [color]=\"progressbarColor || 'success'\" [value]=\"value\"></ion-progress-bar>\n    </div>\n    <ng-content></ng-content>\n</div>",
                styles: [".main-wrapper{margin-top:var(--boxx-progress-container-margin-top,24px);margin-bottom:var(--boxx-progress-container-margin-bottom,32px);margin-left:var(--boxx-progress-container-margin-left,16px);margin-right:var(--boxx-progress-container-margin-right,16px);color:var(--boxx-progress-container-color,var(--ion-color-primary));text-align:var(--boxx-progress-container-align,center);font-size:15px;font-weight:600}.main-wrapper .progress-wrapper{color:var(--boxx-progress-text-color,#fff);text-align:var(--boxx-progress-text-align,right);font-size:12px;font-weight:500}.main-wrapper .progress-wrapper .text{padding:var(--boxx-progress-text-padding,0);margin:var(--boxx-progress-text-margin,0 0 4px 0)}.main-wrapper .progress-wrapper ion-progress-bar{height:var(--boxx-progress-bar-height,8px);background:var(--boxx-progress-bar-background,#fff)}.main-wrapper ::ng-deep ion-button{margin:var(--boxx-progress-button-margin,16px 0)}.main-wrapper ::ng-deep ion-button:last-of-type{margin-bottom:32px}"]
            })
        ], ModuleProgressItemComponent);
        return ModuleProgressItemComponent;
    }());

    var KpiItemComponent = /** @class */ (function () {
        function KpiItemComponent() {
        }
        KpiItemComponent.prototype.ngOnInit = function () { };
        KpiItemComponent = __decorate([
            core.Component({
                selector: 'boxx-kpi-item',
                template: "<p>\n  kpi-item works!\n</p>\n",
                styles: [""]
            })
        ], KpiItemComponent);
        return KpiItemComponent;
    }());

    var WidgetHeaderItemComponent = /** @class */ (function () {
        function WidgetHeaderItemComponent() {
        }
        WidgetHeaderItemComponent.prototype.ngOnInit = function () { };
        WidgetHeaderItemComponent = __decorate([
            core.Component({
                selector: 'boxx-widget-header-item',
                template: "<p>\n  widget-header-item works!\n</p>\n",
                styles: [""]
            })
        ], WidgetHeaderItemComponent);
        return WidgetHeaderItemComponent;
    }());

    var SharedComponentsModule = /** @class */ (function () {
        function SharedComponentsModule() {
        }
        SharedComponentsModule = __decorate([
            core.NgModule({
                imports: [
                    common.CommonModule,
                    forms.FormsModule,
                    angular.IonicModule,
                    core$1.TranslateModule.forChild()
                ],
                declarations: [
                    // ------ GENERAL-CONTENT COMPONENTS:
                    MainHeaderIconComponent,
                    ContentHeaderItemComponent,
                    ContentDividerComponent,
                    CustomModalComponent,
                    CountryPickerComponent,
                    LanguagePickerComponent,
                    LocationPickerComponent,
                    // ------ BUSINESS COMPONENTS:
                    CardConfigurationFormComponent,
                    // ------ EVENTS COMPONENTS:
                    AppointmentItemComponent,
                    // ------ MESSAGES COMPONENTS:
                    ConversationItemComponent,
                    // ------ ONBOARDING COMPONENTS:
                    HiddenInputItemComponent,
                    ModuleProgressItemComponent,
                    // ------ ONBOARDING COMPONENTS:
                    KpiItemComponent,
                    WidgetHeaderItemComponent,
                ],
                exports: [
                    // ------ GENERAL-CONTENT COMPONENTS:
                    MainHeaderIconComponent,
                    ContentHeaderItemComponent,
                    ContentDividerComponent,
                    CustomModalComponent,
                    CountryPickerComponent,
                    LanguagePickerComponent,
                    LocationPickerComponent,
                    // ------ BUSINESS COMPONENTS:
                    CardConfigurationFormComponent,
                    // ------ EVENTS COMPONENTS:
                    AppointmentItemComponent,
                    // ------ MESSAGES COMPONENTS:
                    ConversationItemComponent,
                    // ------ ONBOARDING COMPONENTS:
                    HiddenInputItemComponent,
                    ModuleProgressItemComponent,
                    // ------ PANEL COMPONENTS:
                    KpiItemComponent,
                    WidgetHeaderItemComponent,
                ]
            })
        ], SharedComponentsModule);
        return SharedComponentsModule;
    }());

    exports.CardConfigurationFormComponent = CardConfigurationFormComponent;
    exports.CountryPickerComponent = CountryPickerComponent;
    exports.LanguagePickerComponent = LanguagePickerComponent;
    exports.LocationPickerComponent = LocationPickerComponent;
    exports.SharedComponentsModule = SharedComponentsModule;
    exports.ɵa = MainHeaderIconComponent;
    exports.ɵb = ContentHeaderItemComponent;
    exports.ɵc = ContentDividerComponent;
    exports.ɵd = CustomModalComponent;
    exports.ɵe = AppointmentItemComponent;
    exports.ɵf = ConversationItemComponent;
    exports.ɵg = HiddenInputItemComponent;
    exports.ɵh = ModuleProgressItemComponent;
    exports.ɵi = KpiItemComponent;
    exports.ɵj = WidgetHeaderItemComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=boxx-shared-components.umd.js.map
