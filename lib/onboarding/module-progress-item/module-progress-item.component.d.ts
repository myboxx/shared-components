import { OnInit } from '@angular/core';
export declare class ModuleProgressItemComponent implements OnInit {
    text: string;
    class: string;
    progressbarColor: string;
    value: number;
    constructor();
    ngOnInit(): void;
}
