import { EventEmitter, OnInit } from '@angular/core';
export declare class HiddenInputItemComponent implements OnInit {
    text: string;
    closeIconName: string;
    closeIconSize: 'small' | 'large' | undefined;
    closeIconSlot: 'start' | 'end';
    whenInputVisibilityChange: EventEmitter<boolean>;
    inputShown: boolean;
    constructor();
    ngOnInit(): void;
    toggleRealInputVisibility(show: boolean): void;
}
