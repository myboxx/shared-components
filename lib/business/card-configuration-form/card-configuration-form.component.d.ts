import { OnInit } from '@angular/core';
export declare class CardConfigurationFormComponent implements OnInit {
    country: {
        name: string;
        flag: string;
    };
    constructor();
    ngOnInit(): void;
    openCountryPicker(): void;
}
