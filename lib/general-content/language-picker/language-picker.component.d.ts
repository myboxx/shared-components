import { OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
export declare class LanguagePickerComponent implements OnInit {
    private modalCtrl;
    languageList: Array<{
        id: string;
        name: string;
        description: string;
        selected: boolean;
    }>;
    selectedId: string;
    constructor(modalCtrl: ModalController);
    ngOnInit(): void;
    closeView(): void;
    selectLanguage(language: {
        id: string;
        name: string;
        description: string;
        selected: boolean;
    }): void;
}
