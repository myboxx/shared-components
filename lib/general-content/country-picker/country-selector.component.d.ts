import { OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ICountryCodes } from '@boxx/contacts-core';
export declare class CountryPickerComponent implements OnInit {
    viewController: ModalController;
    countries: Array<ICountryCodes>;
    items: any[];
    constructor(viewController: ModalController);
    ngOnInit(): void;
    select(country: ICountryCodes): void;
    search(searchTerm: string): void;
    close(): void;
}
