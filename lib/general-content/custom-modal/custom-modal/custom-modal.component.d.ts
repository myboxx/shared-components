import { OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
export declare class CustomModalComponent implements OnInit {
    private popoverController;
    constructor(popoverController: ModalController);
    ngOnInit(): void;
    close(): void;
}
