import { OnDestroy } from '@angular/core';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
export declare class LocationPickerComponent implements OnDestroy {
    private modalCtrl;
    searchBar: IonSearchbar;
    destroyed$: Subject<boolean>;
    isLoading$: Observable<boolean>;
    searchResults: Array<any>;
    translations: any;
    mapsService: any;
    constructor(modalCtrl: ModalController);
    ngOnDestroy(): void;
    ionViewDidEnter(): void;
    onInput(query: string): void;
    close(): Promise<void>;
    chooseItem(item: any): void;
    select(): void;
}
